/********************************************************************
*
*	WRAPPER		:: InterfaceDesignerWrapper
*	DESCRIPTION	:: Exposes functions from an unmanaged C++ library so they can be used in a C#
*					application.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2015 / 07 / 24
*
********************************************************************/

#ifndef InterfaceDesignerWrapperH
#define InterfaceDesignerWrapperH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include <Windows.h>

/********************************************************************
*	Defines and constants.
********************************************************************/
/// Allows a parameter to be optional and have a default value set (same as C++'s 'float x = 1.0f').
#define ParamDefault	[System::Runtime::InteropServices::Optional]
/// Sets a parameter to expect a different value to be returned (same as C++'s 'float& x').
#define ParamOut		[System::Runtime::InteropServices::Out]


public ref class InterfaceDesignerWrapper {
	public:
		/// Initialises DirectX.
		bool _Initialise( System::IntPtr hWnd);
		/// Updates and renders the current scene.
		bool _UpdateAndRender();
		/// Closes DirectX.
		bool _Shutdown();

		/// Saves the current interface to the specified file.
		bool _SaveInterface( System::String^ file);
		/// Loads an interface from the specified file.
		bool _LoadInterface( System::String^ file);

		/// Gets the current number of objects in the scene.
		bool _GetObjectCount( ParamOut int %count);
		/// Gets the object at the specified index.
		bool _GetObjectAtIndex( int index, ParamOut int %id, ParamOut System::String^ %name);

		/// Adds a button object to the interface.
		bool _AddObjectButton( ParamOut int %id, System::String^ name, float x, float y);
		/// Adds an icon object to the interface.
		bool _AddObjectIcon( ParamOut int %id, System::String^ name, float x, float y);
		/// Adds a text object to the interface.
		bool _AddObjectText( ParamOut int %id, System::String^ name, float x, float y);
		/// Deletes an object from the interface using it's unique ID.
		bool _DeleteObject( int id);
		/// Deletes all objects from the interface.
		bool _DeleteAllObjects();
		
		/// Iterates through the names of events that objects have.
		bool _GetObjectEventNames( int index, ParamOut System::String^ %eventName, ParamOut int %eventCount);
		/// Gets the maximum string length permitted for Lua event scripts.
		bool _GetObjectEventStringLimit( ParamOut int %limit);
		
		/// Gets an object's name from its ID.
		bool _GetObjectNameFromID( int id, ParamOut System::String^ %name);
		/// Gets an object's type (in string form) from its ID.
		bool _GetObjectTypeFromID( int id, ParamOut System::String^ %type);
		/// Gets an object's location from its ID.
		bool _GetObjectLocationFromID( int id, ParamOut float %x, ParamOut float %y, ParamOut float %z);
		/// Gets an object's dimensions from its ID.
		bool _GetObjectSizeFromID( int id, ParamOut float %width, ParamOut float %height);
		/// Gets an object's texture path from its ID.
		bool _GetObjectTexturePathFromID( int id, ParamOut System::String^ %texturePath);
		/// Gets an object's text.
		bool _GetObjectTextFromID( int id, ParamOut System::String^ %text);
		/// Gets an object's font's colour from its ID.
		bool _GetObjectFontColourFromID( int id, ParamOut int %red, ParamOut int %green, ParamOut int %blue, ParamOut int %alpha);
		/// Gets an object's event's Lua script.
		bool _GetObjectEventScriptFromID( int id, System::String^ eventName, ParamOut System::String^ %script);
		
		/// Sets an object's name from its ID.
		bool _SetObjectNameFromID( int id, System::String^ name);
		/// Sets an object's location from its ID.
		bool _SetObjectLocationFromID( int id, float x, float y, float z);
		/// Sets an object's dimensions from its ID.
		bool _SetObjectSizeFromID( int id, float width, float height);
		/// Sets an object's texture path from its ID.
		bool _SetObjectTexturePathFromID( int id, System::String^ texturePath);
		/// Sets an object's text.
		bool _SetObjectTextFromID( int id, System::String^ text);
		/// Sets an object's font's colour from its ID.
		bool _SetObjectFontColourFromID( int id, int red, int green, int blue, int alpha);
		/// Sets an object's event's Lua script.
		bool _SetObjectEventScriptFromID( int id, System::String^ eventName, System::String^ script);

		/// Check if the mouse cursor is over an object.
		bool _TestCursorIsOverObject( float x, float y, ParamOut int %id);
		/// Gets what type of cursor should be displayed.
		bool _TestCursorPosOverObject( float x, float y, ParamOut int %type);
		/// Gets the currently selected object.
		bool _GetSelectedObject( ParamOut int %id);
		/// Sets the currently selected object.
		bool _SetSelectedObject( int id);

		/// Moves an object using its ID.
		bool _MoveObjectBy( int id, float x, float y);
		/// Scales an object using its ID.
		bool _ScaleObjectBy( int id, float x, float y, int dir);

		/// Installs a font.
		bool _InstallFont( System::String^ fontPath);
		
	private:


};

/*******************************************************************/
#endif	// #ifndef InterfaceDesignerWrapperH