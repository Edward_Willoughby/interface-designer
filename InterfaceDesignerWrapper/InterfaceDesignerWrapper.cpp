/********************************************************************
*	Function definitions for the InterfaceDesignerWrapper.
********************************************************************/

/// This NEEDS to be at the very top, otherwise weird bugs start happening.
#include "stdafx.h"

/********************************************************************
*	Include header file.
********************************************************************/
#include "InterfaceDesignerWrapper.h"

#include <DBEApp.h>
#include <InterfaceDesignerLib.h>
#include <InterfaceObject.h>

/********************************************************************
*	Namespaces, defines, constants and local variables.
********************************************************************/
using namespace System;
using namespace System::Runtime::InteropServices;

/// The pointer to DirectX (from the C++ library).
APP_DECLARE( InterfaceDesignerLib);
static InterfaceDesignerLib* gsp_directX = nullptr;
static bool gsp_dynamicallyAllocated = false;

/// Macro to ensure that DirectX has been initialised before trying to use it.
#define APP_EXISTS() {			\
	if( GET_APP() == nullptr)	\
		return false;			\
}

/// Macro to convert a String^ into a char*.
#define ConvertStringToChar( x) static_cast<char*>( Marshal::StringToHGlobalAnsi( x).ToPointer())
/// Macro to convert a char* to String^
#define ConvertCharToString( x) %String( x)


/**
* Initialises DirectX by creating an instance of 'Application' and storing it in 'gp_directX' so it
* can be continually accessed.
*
* @param hwnd :: The handle of the panel in C# that DirectX will be rendering to.
*
* @return True if DirectX was initialised with no errors.
*/
bool InterfaceDesignerWrapper::_Initialise( IntPtr hwnd) {
	// Only initialise DirectX if the local pointer is currently 'nullptr', otherwise it'll
	// overwrite an existing instance and lose track of it (causing all sorts of memory leaks and
	// potential errors).
	
	if( GET_APP() == nullptr) {
		gsp_directX = new InterfaceDesignerLib();
		GET_APP() = gsp_directX;
		gsp_dynamicallyAllocated = true;
	}

	gsp_directX = dynamic_cast<InterfaceDesignerLib*>( GET_APP());
	return gsp_directX->Init( 0x0000FFFF, HWND( hwnd.ToPointer()));
}

/**
* Refreshes DirectX and updates the 'picture' being sent by the library.
*
* @return True if DirectX was updated and rendered with no errors.
*/
bool InterfaceDesignerWrapper::_UpdateAndRender() {
	APP_EXISTS();

	return gsp_directX->RunOnce();
}

/**
* Closes DirectX and releases any memory that it may've been using.
*
* @return True if DirectX closed with no errors.
*/
bool InterfaceDesignerWrapper::_Shutdown() {
	// If DirectX isn't initialised, then don't try to close it.
	if( GET_APP() == nullptr)
		return true;

	gsp_directX->Shutdown();

	if( gsp_dynamicallyAllocated)
		delete gsp_directX;
	gsp_directX = nullptr;

	return true;
}

/**
* Saves the current interface to the specified file.
*
* @param file :: The path to save the interface to.
*
* @return True if no errors occurred.
*/
bool InterfaceDesignerWrapper::_SaveInterface( System::String^ file) {
	APP_EXISTS();

	const char* f = ConvertStringToChar( file);
	bool res = false;

	res = gsp_directX->SaveInterface( f);

	return res;
}

/**
* Loads an interface from the specified file.
*
* @param file :: The file to load the interface from.
*
* @return True if no errors occurred.
*/
bool InterfaceDesignerWrapper::_LoadInterface( System::String^ file) {
	APP_EXISTS();

	const char* f = ConvertStringToChar( file);
	bool res = false;

	res = gsp_directX->LoadInterface( f);

	return res;
}

/**
* Gets the current number of objects in the interface.
*
* @param count :: The number of objects in the interface.
*
* @return True if no errors occurred.
*/
bool InterfaceDesignerWrapper::_GetObjectCount( ParamOut int %count) {
	APP_EXISTS();

	int c = 0;
	bool res = false;

	res = gsp_directX->GetObjectCount( c);

	count = c;

	return res;
}

/**
* Gets the object at the specified index.
*
* @param index	:: The position of the object in the library's array.
* @param id		:: The variable to hold the object's unique ID.
* @param name	:: The variable to hold the object's name.
*
* @return True if no errors occurred.
*/
bool InterfaceDesignerWrapper::_GetObjectAtIndex( int index, ParamOut int %id, ParamOut System::String^ %name) {
	APP_EXISTS();

	int ind = index;
	int i = id;
	const char* n = nullptr;
	bool res = false;

	res = gsp_directX->GetObjectAtIndex( ind, i, n);

	id = i;
	name = ConvertCharToString( n);

	return res;
}

/**
* Adds a button object to the interface.
*
* @param x :: The x position to place the object.
* @param y :: The y position to place the object.
*
* @return True if no errors occurred.
*/
bool InterfaceDesignerWrapper::_AddObjectButton( ParamOut int %id, System::String^ name, float x, float y) {
	APP_EXISTS();

	char* n = ConvertStringToChar( name);
	int i = 0;
	bool res = false;

	res = gsp_directX->AddObjectButton( i, n, x, y);

	name = ConvertCharToString( n);
	id = i;

	return res;
}

/**
* Adds an icon object to the interface.
*
* @param x :: The x position to place the object.
* @param y :: The y position to place the object.
*
* @return True if no errors occurred.
*/
bool InterfaceDesignerWrapper::_AddObjectIcon( ParamOut int %id, System::String^ name, float x, float y) {
	APP_EXISTS();

	char* n = ConvertStringToChar( name);
	int i = 0;
	bool res = false;

	res = gsp_directX->AddObjectIcon( i, n, x, y);

	name = ConvertCharToString( n);
	id = i;

	return res;
}

/**
* Adds a text object to the interface.
*
* @param x :: The x position to place the object.
* @param y :: The y position to place the object.
*
* @return True if no errors occurred.
*/
bool InterfaceDesignerWrapper::_AddObjectText( ParamOut int %id, System::String^ name, float x, float y) {
	APP_EXISTS();

	char* n = ConvertStringToChar( name);
	int i = 0;
	bool res = false;

	res = gsp_directX->AddObjectText( i, n, x, y);

	id = i;

	return res;
}

/**
* Deletes an object from the interface using it's unique ID.
*
* @param id :: The object's unique ID.
*
* @return True if no errors occurred.
*/
bool InterfaceDesignerWrapper::_DeleteObject( int id) {
	APP_EXISTS();

	int i = id;
	bool res = false;

	res = gsp_directX->DeleteObject( i);

	return res;
}

/**
* Deletes all objects from the interface.
*
* @return True if no errors occurred.
*/
bool InterfaceDesignerWrapper::_DeleteAllObjects() {
	APP_EXISTS();

	return gsp_directX->DeleteAllObjects();
}

/**
* Iterates through the names of events that objects have.
*
* @param index		:: The index of the event to get the name of.
* @param eventName	:: The variable to hold the event's name.
* @param eventCount	:: The variable to hold the number of events.
*
* @return True if no errors occurred.
*/
bool InterfaceDesignerWrapper::_GetObjectEventNames( int index, ParamOut System::String^ %eventName, ParamOut int %eventCount) {
	APP_EXISTS();

	const char* n = nullptr;
	int i = index;
	int c = 0;
	bool res = false;

	res = gsp_directX->GetObjectEventNames( i, n, c);

	eventName = ConvertCharToString( n);
	eventCount = c;

	return res;
}

/**
* Gets the maximum string length permitted for Lua event scripts.
*
* @param limit :: The variable to hold the maximum length.
*
* @return True if no errors occurred.
*/
bool InterfaceDesignerWrapper::_GetObjectEventStringLimit( int %limit) {
	APP_EXISTS();

	int l = 0;
	bool res = false;

	res = gsp_directX->GetObjectEventStringLimit( l);

	limit = l;

	return res;
}

/**
* Gets an object's name from its ID.
*
* @param id		:: The object's unique ID.
* @param name	:: The variable to hold the object's name.
*
* @return True if no errors occurred.
*/
bool InterfaceDesignerWrapper::_GetObjectNameFromID( int id, ParamOut System::String^ %name) {
	APP_EXISTS();

	const char* n = nullptr;
	int i = id;
	bool res = false;

	res = gsp_directX->GetObjectNameFromID( i, n);

	name = ConvertCharToString( n);

	return res;
}

/**
* Gets an object's type (in string form) from its ID.
*
* @param id		:: The object's unique ID.
* @param type	:: The variable to hold the object's type.
*
* @return True if no errors occurred.
*/
bool InterfaceDesignerWrapper::_GetObjectTypeFromID( int id, ParamOut System::String^ %type) {
	APP_EXISTS();

	char t[256];
	int i = id;
	bool res = false;

	res = gsp_directX->GetObjectTypeFromID( i, t);

	type = ConvertCharToString( t);

	return res;
}

/**
* Gets an object's location from its ID.
*
* @param id	:: The object's unique ID.
* @param x	:: The variable to hold the object's x position.
* @param y	:: The variable to hold the object's y position.
* @param z	:: The variable to hold the object's z position.
*
* @return True if no errors occurred.
*/
bool InterfaceDesignerWrapper::_GetObjectLocationFromID( int id, ParamOut float %x, ParamOut float %y, ParamOut float %z) {
	APP_EXISTS();

	int i = id;
	float px = 0.0f;
	float py = 0.0f;
	float pz = 0.0f;
	bool res = false;

	res = gsp_directX->GetObjectLocationFromID( i, px, py, pz);

	x = px;
	y = py;
	z = pz;

	return res;
}

/**
* Gets an object's dimensions from its ID.
*
* @param id		:: The object's unique ID.
* @param width	:: The variable to hold the object's x dimension.
* @param height	:: The variable to hold the object's y dimension.
*
* @return True if no errors occurred.
*/
bool InterfaceDesignerWrapper::_GetObjectSizeFromID( int id, ParamOut float %width, ParamOut float %height) {
	APP_EXISTS();

	int i = id;
	float x = 0.0f;
	float y = 0.0f;
	bool res = false;

	res = gsp_directX->GetObjectSizeFromID( i, x, y);

	width = x;
	height = y;

	return res;
}

/**
* Gets an object's texture path from its ID.
*
* @param id				:: The object's unique ID.
* @param texturePath	:: The variable to hold the object's texture's path.
*
* @return True if no errors occurred.
*/
bool InterfaceDesignerWrapper::_GetObjectTexturePathFromID( int id, ParamOut System::String^ %texturePath) {
	APP_EXISTS();

	int i = id;
	const char* tp = nullptr;
	bool res = false;

	res = gsp_directX->GetObjectTexturePathFromID( i, tp);

	texturePath = ConvertCharToString( tp);

	return res;
}

/**
* Gets an object's text.
*
* @param id				:: The object's unique ID.
* @param texturePath	:: The variable to hold the object's text.
*
* @return True if no errors occurred.
*/
bool InterfaceDesignerWrapper::_GetObjectTextFromID( int id, ParamOut System::String^ %text) {
	APP_EXISTS();

	int i = id;
	const char* t = nullptr;
	bool res = false;

	res = gsp_directX->GetObjectTextFromID( i, t);

	text = ConvertCharToString( t);

	return res;
}

/**
* Gets an object's font's colour from its ID.
*
* @param id		:: The object's unique ID.
* @param red	:: The variable to hold the object's font's colour's red value.
* @param green	:: The variable to hold the object's font's colour's green value.
* @param blue	:: The variable to hold the object's font's colour's blue value.
* @param alpha	:: The variable to hold the object's font's colour's alpha value.
*
* @return True if no errors occurred.
*/
bool InterfaceDesignerWrapper::_GetObjectFontColourFromID( int id, ParamOut int %red, ParamOut int %green, ParamOut int %blue, ParamOut int %alpha) {
	APP_EXISTS();

	int i = id;
	int r = 0;
	int g = 0;
	int b = 0;
	int a = 0;
	bool res = false;

	res = gsp_directX->GetObjectFontColourFromID( id, r, g, b, a);

	red = r;
	green = g;
	blue = b;
	alpha = a;

	return res;
}

/**
* Gets an object's event's Lua script.
*
* @param id			:: The object's unique ID.
* @param eventName	:: The name of the event.
* @param script		:: The variable to hold the object's event's Lua script.
*
* @return True if no errors occurred.
*/
bool InterfaceDesignerWrapper::_GetObjectEventScriptFromID( int id, System::String^ eventName, ParamOut System::String^ %script) {
	APP_EXISTS();

	int i = id;
	const char* e = ConvertStringToChar( eventName);
	char s[gsc_luaScriptLength];
	bool res = false;

	res = gsp_directX->GetObjectEventScriptFromID( i, e, s);

	script = ConvertCharToString( s);

	return res;
}

/**
* Sets an object's name from its ID.
*
* @param id		:: The object's unique ID.
* @param name	:: The object's new name.
*
* @return True if no errors occurred.
*/
bool InterfaceDesignerWrapper::_SetObjectNameFromID( int id, System::String^ name) {
	APP_EXISTS();

	const char* n = ConvertStringToChar( name);
	int i = id;
	bool res = false;

	res = gsp_directX->SetObjectNameFromID( i, n);

	return res;
}

/**
* Sets an object's location from its ID.
*
* @param id	:: The object's unique ID.
* @param x	:: The object's new x position.
* @param y	:: The object's new y position.
* @param z	:: The object's new z position.
*
* @return True if no errors occurred.
*/
bool InterfaceDesignerWrapper::_SetObjectLocationFromID( int id, float x, float y, float z) {
	APP_EXISTS();

	int i = id;
	float px = x;
	float py = y;
	float pz = z;
	bool res = false;

	res = gsp_directX->SetObjectLocationFromID( i, px, py, pz);

	return res;
}

/**
* Sets an object's dimensions from its ID.
*
* @param id		:: The object's unique ID.
* @param width	:: The object's new width.
* @param height	:: The object's new height.
*
* @return True if no errors occurred.
*/
bool InterfaceDesignerWrapper::_SetObjectSizeFromID( int id, float width, float height) {
	APP_EXISTS();

	int i = id;
	float x = width;
	float y = height;
	bool res = false;

	res = gsp_directX->SetObjectSizeFromID( i, x, y);

	return res;
}

/**
* Sets an object's texture path from its ID.
*
* @param id				:: The object's unique ID.
* @param texturePath	:: The object's new texture.
*
* @return True if no errors occurred.
*/
bool InterfaceDesignerWrapper::_SetObjectTexturePathFromID( int id, System::String^ texturePath) {
	APP_EXISTS();

	int i = id;
	const char* tp = ConvertStringToChar( texturePath);
	bool res = false;

	res = gsp_directX->SetObjectTexturePathFromID( i, tp);

	return res;
}

/**
* Sets an object's text.
*
* @param id				:: The object's unique ID.
* @param texturePath	:: The object's new text.
*
* @return True if no errors occurred.
*/
bool InterfaceDesignerWrapper::_SetObjectTextFromID( int id, System::String^ text) {
	APP_EXISTS();

	int i = id;
	const char* t = ConvertStringToChar( text);
	bool res = false;

	res = gsp_directX->SetObjectTextFromID( i, t);

	return res;
}

/**
* Sets an object's font's colour from its ID.
*
* @param id		:: The object's unique ID.
* @param red	:: The object's font's colour's new red value.
* @param green	:: The object's font's colour's new green value.
* @param blue	:: The object's font's colour's new blue value.
* @param alpha	:: The object's font's colour's new alpha value.
*
* @return True if no errors occurred.
*/
bool InterfaceDesignerWrapper::_SetObjectFontColourFromID( int id, int red, int green, int blue, int alpha) {
	APP_EXISTS();

	int i = id;
	int r = red;
	int g = green;
	int b = blue;
	int a = alpha;
	bool res = false;

	res = gsp_directX->SetObjectFontColourFromID( id, r, g, b, a);

	return res;
}

/**
* Sets an object's event's Lua script.
*
* @param id			:: The object's unique ID.
* @param eventName	:: The name of the event.
* @param script		:: The object's event's new Lua script.
*
* @return True if no errors occurred.
*/
bool InterfaceDesignerWrapper::_SetObjectEventScriptFromID( int id, System::String^ eventName, System::String^ script) {
	APP_EXISTS();

	int i = id;
	const char* e = ConvertStringToChar( eventName);
	char* s = ConvertStringToChar( script);
	bool res = false;

	res = gsp_directX->SetObjectEventScriptFromID( i, e, s);

	return res;
}

/**
* Check if the mouse cursor is over an object.
*
* @param x	:: The x position of the cursor.
* @param y	:: The y position of the cursor.
* @param id	:: The variable to hold the object's unique ID.
*
* @return True if no errors occurred.
*/
bool InterfaceDesignerWrapper::_TestCursorIsOverObject( float x, float y, ParamOut int %id) {
	APP_EXISTS();

	float px = x;
	float py = y;
	int i = 0;
	bool res = false;

	res = gsp_directX->TestCursorIsOverObject( px, py, i);

	id = i;

	return res;
}

/**
* Gets what type of cursor should be displayed.
*
* @param x	:: The x position of the cursor.
* @param y	:: The y position of the cursor.
* @param id	:: The variable to hold the cursor type.
*
* @return True if no errors occurred.
*/
bool InterfaceDesignerWrapper::_TestCursorPosOverObject( float x, float y, ParamOut int %type) {
	APP_EXISTS();

	float px = x;
	float py = y;
	int t = 0;
	bool res = false;

	res = gsp_directX->TestCursorPosOverObject( px, py, t);

	type = t;

	return res;
}

/**
* Gets the currently selected object.
*
* @param id :: The variable to hold the object's unique id.
*
* @return True if no errors occurred.
*/
bool InterfaceDesignerWrapper::_GetSelectedObject( ParamOut int %id) {
	APP_EXISTS();

	int i = 0;
	bool res = false;

	res = gsp_directX->GetSelectedObject( i);

	id = i;

	return res;
}

/**
* Sets the selected object.
*
* @param id :: The object's unique id.
*
* @return True if no errors occurred.
*/
bool InterfaceDesignerWrapper::_SetSelectedObject( int id) {
	APP_EXISTS();

	int i = id;
	bool res = false;

	res = gsp_directX->SetSelectedObject( id);

	return res;
}

/**
* Moves an object using its ID.
*
* @param id	:: The object's unique ID.
* @param x	:: The amount to move the object by.
* @param y	:: The amount to move the object by.
*
* @return True if no errors occurred.
*/
bool InterfaceDesignerWrapper::_MoveObjectBy( int id, float x, float y) {
	APP_EXISTS();

	int i = id;
	float px = x;
	float py = y;
	bool res = false;

	res = gsp_directX->MoveObjectBy( i, px, py);

	return res;
}

/**
* Scales an object using its ID.
*
* @param id		:: The object's unique ID.
* @param x		:: The amount to scale the object by.
* @param y		:: The amount to scale the object by.
* @param dir	:: The direction to scale in (should be the cursor type).
*
* @return True if no errors occurred.
*/
bool InterfaceDesignerWrapper::_ScaleObjectBy( int id, float x, float y, int dir) {
	APP_EXISTS();

	int i = id;
	float px = x;
	float py = y;
	int d = dir;
	bool res = false;

	res = gsp_directX->ScaleObjectBy( i, px, py, d);

	return res;
}

/**
* Installs a font.
*
* @param fontPath :: The path to the font to be installed.
*
* @return True if no errors occurred. False means that either the font already exists, or it
*			couldn't be installed due to permission or an incorrect path.
*/
bool InterfaceDesignerWrapper::_InstallFont( System::String^ fontPath) {
	APP_EXISTS();

	const char* f = ConvertStringToChar( fontPath);
	bool res = false;

	res = gsp_directX->InstallFont( f);

	return res;
}