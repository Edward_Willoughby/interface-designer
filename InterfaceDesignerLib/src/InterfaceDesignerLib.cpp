/********************************************************************
*	Function definitions for the InterfaceDesignerLib class.
********************************************************************/

/********************************************************************
*	Include the header file.
********************************************************************/
#include "InterfaceDesignerLib.h"

#include <DBECamera.h>
#include <DBEFont.h>
#include <DBEUITexture.h>

#include <InterfaceContainer.h>
#include <InterfaceObject.h>
/********************************************************************
*	Defines, constants, namespaces, and local variables.
********************************************************************/
using namespace DBE;
using namespace DirectX;


/**
* Deals with initialisation.
*
* @return True if everything initialised correctly.
*/
bool InterfaceDesignerLib::HandleInit() {
	mp_selection = new UITexture( "texture_selection.dds", GET_APP()->GetSamplerState());
	m_objSelected = -1;

	mp_interface = new InterfaceContainer();
	if( !mp_interface->Init( this, InterfaceFuncPointers()))
		return false;

	// Prevent the 'InterfaceContainer' from attempting to check for events or run scripts.
	mp_interface->UseLuaScripts( false);
	mp_interface->EnableObjectEvents( false);

	// Prevent the application from doing anything with the cursor; the C# tool will deal with it.
	this->DeferCursorControl();

	return true;
}

/**
* Deals with any updating.
*
* @param deltaTime :: The time taken to render the last frame.
*
* @return True if everything updated correctly.
*/
bool InterfaceDesignerLib::HandleUpdate( float deltaTime) {
	return mp_interface->Update( deltaTime);
}

/**
* Deals with any rendering.
*
* @param deltaTime :: The time taken to render the last frame.
*
* @return True if everything rendered correctly.
*/
bool InterfaceDesignerLib::HandleRender( float deltaTime) {
	bool res = mp_interface->Render( deltaTime);

	if( m_objSelected != -1) {
		InterfaceObject* p_obj( mp_interface->GetObjectByID( m_objSelected));
		Vec2 dim( p_obj->GetBoundingDimensions());
		Vec3 pos( p_obj->GetPosition());

		if( p_obj->GetType() == InterfaceObjectType::IO_Text) {
			pos.SetX( pos.GetX() + (dim.GetX() / 2.0f));
			pos.SetY( pos.GetY() + (dim.GetY() / 2.0f));
			pos.SetZ( 10.0f);
		}

		mp_selection->MoveTo( pos);
		mp_selection->ScaleTo( dim.GetX(), dim.GetY(), 1.0f);

		mp_selection->Render( deltaTime);
	}

	return res;
}

/**
* Deals with anything that needs to be released or shutdown.
*/
bool InterfaceDesignerLib::HandleShutdown() {
	SafeDelete( mp_selection);

	mp_interface->Shutdown();
	SafeDelete( mp_interface);
	
	return true;
}

/**
* Saves the current interface to the specified file.
*
* @param file :: The path to save the interface to.
*
* @return True if no errors occurred.
*/
bool InterfaceDesignerLib::SaveInterface( const char* file) const {
	return mp_interface->SaveInterface( file);
}

/**
* Loads an interface from the specified file.
*
* @param file :: The file to load the interface from.
*
* @return True if no errors occurred.
*/
bool InterfaceDesignerLib::LoadInterface( const char* file) {
	this->DeleteAllObjects();

	return mp_interface->LoadInterface( file);
}

/**
* Gets the current number of objects in the interface.
*
* @param count :: The number of objects in the interface.
*
* @return True if no errors occurred.
*/
bool InterfaceDesignerLib::GetObjectCount( s32& count) {
	count = mp_interface->GetNumberOfObjects();
	return true;
}

/**
* Gets the object at the specified index.
*
* @param index	:: The position of the object in the library's array.
* @param id		:: The variable to hold the object's unique ID.
* @param name	:: The variable to hold the object's name.
*
* @return True if no errors occurred.
*/
bool InterfaceDesignerLib::GetObjectAtIndex( s32 index, s32& id, const char* &name) {
	InterfaceObject* obj( mp_interface->GetObjectAt( index));

	if( obj == nullptr)
		return false;

	id = obj->GetUniqueID();
	name = obj->GetName();

	return true;
}

/**
* Adds a button object to the interface.
*
* @param x :: The x position to place the object.
* @param y :: The y position to place the object.
*
* @return True if no errors occurred.
*/
bool InterfaceDesignerLib::AddObjectButton( s32& id, const char* name, float x, float y) {
	return this->AddNewObject( id, name, InterfaceObjectType::IO_Button, x, y);
}

/**
* Adds an icon object to the interface.
*
* @param x :: The x position to place the object.
* @param y :: The y position to place the object.
*
* @return True if no errors occurred.
*/
bool InterfaceDesignerLib::AddObjectIcon( s32& id, const char* name, float x, float y) {
	return this->AddNewObject( id, name, InterfaceObjectType::IO_Icon, x, y);
}

/**
* Adds a text object to the interface.
*
* @param x :: The x position to place the object.
* @param y :: The y position to place the object.
*
* @return True if no errors occurred.
*/
bool InterfaceDesignerLib::AddObjectText( s32& id, const char* name, float x, float y) {
	return this->AddNewObject( id, name, InterfaceObjectType::IO_Text, x, y);
}

/**
* Deletes an object from the interface using it's unique ID.
*
* @param id :: The object's unique ID.
*
* @return True if no errors occurred. False will be returned if the 'id' doesn't exist; as an
*			invalid ID SHOULDN'T be passed through.
*/
bool InterfaceDesignerLib::DeleteObject( s32 id) {
	m_objSelected = -1;
	return mp_interface->DeleteObject( id);
}

/**
* Deletes all objects from the interface.
*
* @return True if no errors occurred.
*/
bool InterfaceDesignerLib::DeleteAllObjects() {
	m_objSelected = -1;
	mp_interface->DeleteAllObjects();
	return true;
}

/**
* Iterates through the names of events that objects have.
* NOTE: 'eventCount' is set to 'OE_Count' which is the number of elements in the array. Ensure that
* when interating through, 'index' STARTS at 0 and ENDS at eventCount - 1.
*
* @param index		:: The index of the event to get the name of.
* @param eventName	:: The variable to hold the event's name.
* @param eventCount	:: The variable to hold the number of events.
*
* @return True if no errors occurred.
*/
bool InterfaceDesignerLib::GetObjectEventNames( s32 index, const char* &eventName, s32& eventCount) {
	eventCount = OE_Count;
	if( index >= eventCount)
		return false;

	eventName = gsc_objEventNames[index];

	return true;
}

/**
* Gets the maximum string length permitted for Lua event scripts.
*
* @param limit :: The variable to hold the maximum length.
*
* @return True if no errors occurred.
*/
bool InterfaceDesignerLib::GetObjectEventStringLimit( s32& limit) {
	limit = gsc_luaScriptLength;

	return true;
}

/**
* Gets an object's name from its ID.
*
* @param id		:: The object's unique ID.
* @param name	:: The variable to hold the object's name.
*
* @return True if no errors occurred.
*/
bool InterfaceDesignerLib::GetObjectNameFromID( s32 id, const char* &name) {
	InterfaceObject* obj( mp_interface->GetObjectByID( id));

	if( obj == nullptr)
		return false;

	name = obj->GetName();
	return true;
}

/**
* Gets an object's type (in string form) from its ID.
*
* @param id		:: The object's unique ID.
* @param type	:: The variable to hold the object's type.
*
* @return True if no errors occurred.
*/
bool InterfaceDesignerLib::GetObjectTypeFromID( s32 id, char* type) {
	InterfaceObject* obj( mp_interface->GetObjectByID( id));

	if( obj == nullptr)
		return false;

	obj->GetTypeString( type, 256);
	return true;
}

/**
* Gets an object's location from its ID.
*
* @param id	:: The object's unique ID.
* @param x	:: The variable to hold the object's x position.
* @param y	:: The variable to hold the object's y position.
* @param z	:: The variable to hold the object's z position.
*
* @return True if no errors occurred.
*/
bool InterfaceDesignerLib::GetObjectLocationFromID( s32 id, float& x, float& y, float& z) {
	InterfaceObject* obj( mp_interface->GetObjectByID( id));

	if( obj == nullptr)
		return false;

	Vec3 v( obj->GetPosition());
	x = v.GetX();
	y = v.GetY();
	z = v.GetZ();

	return true;
}

/**
* Gets an object's dimensions from its ID.
*
* @param id		:: The object's unique ID.
* @param width	:: The variable to hold the object's x dimension.
* @param height	:: The variable to hold the object's y dimension.
*
* @return True if no errors occurred.
*/
bool InterfaceDesignerLib::GetObjectSizeFromID( s32 id, float& width, float& height) {
	InterfaceObject* obj( mp_interface->GetObjectByID( id));

	if( obj == nullptr)
		return false;

	Vec3 v( obj->GetScale());
	width = v.GetX();
	height = v.GetY();

	return true;
}

/**
* Gets an object's texture path from its ID.
*
* @param id				:: The object's unique ID.
* @param texturePath	:: The variable to hold the object's texture's path.
*
* @return True if no errors occurred.
*/
bool InterfaceDesignerLib::GetObjectTexturePathFromID( s32 id, const char* &texturePath) {
	InterfaceObject* obj( mp_interface->GetObjectByID( id));

	if( obj == nullptr)
		return false;

	texturePath = obj->GetTexture()->m_fileName;

	return true;
}

/**
* Gets an object's text.
*
* @param id				:: The object's unique ID.
* @param texturePath	:: The variable to hold the object's text.
*
* @return True if no errors occurred.
*/
bool InterfaceDesignerLib::GetObjectTextFromID( s32 id, const char* &text) {
	InterfaceObject* obj( mp_interface->GetObjectByID( id));

	if( obj == nullptr)
		return false;

	text = obj->GetText();

	return true;
}

/**
* Gets an object's font's colour from its ID.
*
* @param id		:: The object's unique ID.
* @param red	:: The variable to hold the object's font's colour's red value.
* @param green	:: The variable to hold the object's font's colour's green value.
* @param blue	:: The variable to hold the object's font's colour's blue value.
* @param alpha	:: The variable to hold the object's font's colour's alpha value.
*
* @return True if no errors occurred.
*/
bool InterfaceDesignerLib::GetObjectFontColourFromID( s32 id, s32& red, s32& green, s32& blue, s32& alpha) {
	InterfaceObject* obj( mp_interface->GetObjectByID( id));

	if( obj == nullptr)
		return false;

	u32 colour( obj->GetFontColour());

	u8 rgba[4];
	ColourToInts( rgba[0], rgba[1], rgba[2], rgba[3], colour);

	red = rgba[0];
	green = rgba[1];
	blue = rgba[2];
	alpha = rgba[3];

	return true;
}

/**
* Gets an object's event's Lua script.
*
* @param id			:: The object's unique ID.
* @param eventName	:: The name of the event.
* @param script		:: The variable to hold the object's event's Lua script.
*
* @return True if no errors occurred.
*/
bool InterfaceDesignerLib::GetObjectEventScriptFromID( s32 id, const char* eventName, char* script) {
	InterfaceObject* obj( mp_interface->GetObjectByID( id));

	if( obj == nullptr)
		return false;

	s32 i( 0);
	for( i; i < OE_Count; ++i)
		if( strcmp( eventName, gsc_objEventNames[i]) == 0)
			break;

	if( i == OE_Count)
		return false;

	strcpy_s( script, 1024, obj->GetEvent( ObjectEvent( i)).luaScript);
	//script = obj->GetEvent( ObjectEvent( i)).luaScript;

	return true;
}

/**
* Sets an object's name from its ID.
*
* @param id		:: The object's unique ID.
* @param name	:: The object's new name.
*
* @return True if no errors occurred.
*/
bool InterfaceDesignerLib::SetObjectNameFromID( s32 id, const char* name) {
	InterfaceObject* obj( mp_interface->GetObjectByID( id));

	if( obj == nullptr)
		return false;

	obj->SetName( name);
	return true;
}

/**
* Sets an object's location from its ID.
*
* @param id	:: The object's unique ID.
* @param x	:: The object's new x position.
* @param y	:: The object's new y position.
* @param z	:: The object's new z position.
*
* @return True if no errors occurred.
*/
bool InterfaceDesignerLib::SetObjectLocationFromID( s32 id, float x, float y, float z) {
	InterfaceObject* obj( mp_interface->GetObjectByID( id));

	if( obj == nullptr)
		return false;

	obj->MoveTo( x, y, z);
	return true;
}

/**
* Sets an object's dimensions from its ID.
*
* @param id		:: The object's unique ID.
* @param width	:: The object's new width.
* @param height	:: The object's new height.
*
* @return True if no errors occurred.
*/
bool InterfaceDesignerLib::SetObjectSizeFromID( s32 id, float width, float height) {
	InterfaceObject* obj( mp_interface->GetObjectByID( id));

	if( obj == nullptr)
		return false;

	obj->ScaleTo( width, height, 1.0f);
	return true;
}

/**
* Sets an object's texture path from its ID.
*
* @param id				:: The object's unique ID.
* @param texturePath	:: The object's new texture.
*
* @return True if no errors occurred.
*/
bool InterfaceDesignerLib::SetObjectTexturePathFromID( s32 id, const char* texturePath) {
	InterfaceObject* obj( mp_interface->GetObjectByID( id));

	if( obj == nullptr)
		return false;

	obj->CreateTexture( texturePath);
	return true;
}

/**
* Sets an object's text.
*
* @param id				:: The object's unique ID.
* @param texturePath	:: The object's new text.
*
* @return True if no errors occurred.
*/
bool InterfaceDesignerLib::SetObjectTextFromID( s32 id, const char* text) {
	InterfaceObject* obj( mp_interface->GetObjectByID( id));

	if( obj == nullptr)
		return false;

	obj->SetText( text);

	return true;
}

/**
* Sets an object's font's colour from its ID.
*
* @param id		:: The object's unique ID.
* @param red	:: The object's font's colour's new red value.
* @param green	:: The object's font's colour's new green value.
* @param blue	:: The object's font's colour's new blue value.
* @param alpha	:: The object's font's colour's new alpha value.
*
* @return True if no errors occurred.
*/
bool InterfaceDesignerLib::SetObjectFontColourFromID( s32 id, s32 red, s32 green, s32 blue, s32 alpha) {
	InterfaceObject* obj( mp_interface->GetObjectByID( id));

	if( obj == nullptr)
		return false;

	obj->SetFontColour( IntsToColour( red, green, blue, alpha));

	return true;
}

/**
* Sets an object's event's Lua script.
*
* @param id			:: The object's unique ID.
* @param eventName	:: The name of the event.
* @param script		:: The object's event's new Lua script.
*
* @return True if no errors occurred.
*/
bool InterfaceDesignerLib::SetObjectEventScriptFromID( s32 id, const char* eventName, char* script) {
	InterfaceObject* obj( mp_interface->GetObjectByID( id));

	if( obj == nullptr)
		return false;

	s32 i( 0);
	for( i; i < OE_Count; ++i)
		if( strcmp( eventName, gsc_objEventNames[i]) == 0)
			break;

	if( i == OE_Count)
		return false;

	Event e( script);
	obj->SetEvent( ObjectEvent( i), e);

	return true;
}

/**
* Check if the mouse cursor is over an object.
*
* @param x	:: The x position of the cursor.
* @param y	:: The y position of the cursor.
* @param id	:: The variable to hold the object's unique ID.
*
* @return True if no errors occurred.
*/
bool InterfaceDesignerLib::TestCursorIsOverObject( float x, float y, s32& id) {
	Vec2 mousePos( x, y);
	s32 count( mp_interface->GetNumberOfObjects());
	InterfaceObject* obj( nullptr);
	s32 closest( -1);
	float closestDist( -1.0f);

	// Use a reverse iterator so that if two objects are under the mouse, the one that is rendered
	// second will be selected (as the object rendered second will be on top of the first).
	ContainerObjectList::const_reverse_iterator it( mp_interface->GetConstRIterator());

	for( s32 i( 0); i < count; ++i, ++it) {
		InterfaceObject* object( (*it));
		if( object == nullptr)
			continue;

		if( object->MouseIsOverObject( mousePos)) {
			float z( object->GetPosition().GetZ());

			if( closest == -1 || z > closestDist) {
				obj = object;
				closest = (count - i) - 1;	// Compensate for the reverse iterator (0 == last item).
				closestDist = z;
			}
		}
	}

	if( closest != -1)
		closest = mp_interface->GetObjectAt( closest)->GetUniqueID();

	id = closest;
	m_objSelected = closest;

	return true;
}

/**
* Gets what type of cursor should be displayed.
*
* @param x	:: The x position of the cursor.
* @param y	:: The y position of the cursor.
* @param id	:: The variable to hold the cursor type.
*
* @return True if no errors occurred.
*/
bool InterfaceDesignerLib::TestCursorPosOverObject( float x, float y, s32& cursorType) {
	Vec2 mousePos( x, y);
	InterfaceObject* obj( nullptr);
	cursorType = CT_None;

	if( m_objSelected == -1)
		return true;

	obj = mp_interface->GetObjectByID( m_objSelected);

	// Early out if the mouse is not over the object.
	if( !obj->MouseIsOverObject( mousePos))
		return true;

	// Text can't be scaled, so just set it to the 'movement' cursor and early out.
	if( obj->GetType() == InterfaceObjectType::IO_Text) {
		cursorType = CT_Middle;
		return true;
	}

	// Determine which section of the object the cursor is closest to.
	Vec3 pos( obj->GetPosition());
	Vec3 scale( obj->GetScale());
	float xScale( (obj->GetScale().GetX() / 2.0f) - (obj->GetScale().GetX() / 5.0f));
	float yScale( (obj->GetScale().GetY() / 2.0f) - (obj->GetScale().GetY() / 5.0f));

	bool top =		(pos.GetY() + yScale) <= mousePos.GetY();
	bool bottom =	(pos.GetY() - yScale) >= mousePos.GetY();
	bool right =	(pos.GetX() + xScale) <= mousePos.GetX();
	bool left =		(pos.GetX() - xScale) >= mousePos.GetX();

	if( top && left)			cursorType = CT_TopLeft;
	else if( top && right)		cursorType = CT_TopRight;
	else if( top)				cursorType = CT_Top;
	else if( bottom && left)	cursorType = CT_BottomLeft;
	else if( bottom && right)	cursorType = CT_BottomRight;
	else if( bottom)			cursorType = CT_Bottom;
	else if( left)				cursorType = CT_Left;
	else if( right)				cursorType = CT_Right;
	else /* in the middle */	cursorType = CT_Middle;

	return true;
}

/**
* Gets the currently selected object.
*
* @param id :: The variable to hold the object's unique id.
*
* @return True if no errors occurred.
*/
bool InterfaceDesignerLib::GetSelectedObject( s32& id) {
	id = m_objSelected;
	return true;
}

/**
* Sets the selected object.
*
* @param id :: The object's unique id.
*
* @return True if no errors occurred.
*/
bool InterfaceDesignerLib::SetSelectedObject( s32 id) {
	m_objSelected = id;
	return true;
}

/**
* Moves an object using its ID.
*
* @param id	:: The object's unique ID.
* @param x	:: The amount to move the object by.
* @param y	:: The amount to move the object by.
*
* @return True if no errors occurred.
*/
bool InterfaceDesignerLib::MoveObjectBy( s32 id, float x, float y) {
	InterfaceObject* obj( mp_interface->GetObjectByID( id));

	if( obj == nullptr)
		return false;

	obj->MoveBy( x, y, 0.0f);

	return true;
}

/**
* Scales an object using its ID.
*
* @param id	:: The object's unique ID.
* @param x		:: The amount to scale the object by.
* @param y		:: The amount to scale the object by.
* @param dir	:: The direction to scale in (should be the cursor type).
*
* @return True if no errors occurred.
*/
bool InterfaceDesignerLib::ScaleObjectBy( s32 id, float x, float y, s32 dir) {
	InterfaceObject* obj( mp_interface->GetObjectByID( id));

	if( obj == nullptr)
		return false;

	// Nullify the x or y axis if that's not meant to be taken into account.
	if( dir == CT_Left || dir == CT_Right)
		y = 0.0f;
	else if( dir == CT_Top || dir == CT_Bottom)
		x = 0.0f;

	// Invert the directions for the bottom and left sides.
	bool left = dir == CT_TopLeft || dir == CT_Left || dir == CT_BottomLeft;
	bool right = dir == CT_Bottom || dir == CT_BottomRight || dir == CT_BottomLeft;
	if( left)	x *= -1.0f;
	if( right)	y *= -1.0f;

	Vec3 scalePrev( obj->GetScale());
	Vec3 scale( scalePrev + Vec3( x, y, 0.0f));
	float minScale( 0.0001f);

	// Clamp the object's scale so it can't be inverted.
	if( scale.GetX() < minScale)	x = scalePrev.GetX() - minScale;
	if( scale.GetY() < minScale)	y = scalePrev.GetY() - minScale;

	obj->ScaleBy( x, y, 0.0f);

	if( dir == CT_Middle || dir == CT_None)
		return true;

	// Invert the directions again because the sign was lost.
	if( left)	x *= -1.0f;
	if( right)	y *= -1.0f;

	// Halve the scale for the movement in the y direction.
	if( dir == CT_Top || dir == CT_TopLeft || dir == CT_TopRight || dir == CT_Bottom || dir == CT_BottomLeft || dir == CT_BottomRight)
		y /= 2.0f;
	else
		y = 0.0f;

	// Halve the scale for the movement in the x direction.
	if( dir == CT_TopLeft || dir == CT_Left || dir == CT_BottomLeft || dir == CT_TopRight || dir == CT_Right || dir == CT_BottomRight)
		x /= 2.0f;
	else
		x = 0.0f;

	obj->MoveBy( x, y, 0.0f);

	return true;
}

/**
* Installs a font.
*
* @param fontPath :: The path to the font to be installed.
*
* @return True if no errors occurred. False means that either the font already exists, or it
*			couldn't be installed due to permission or an incorrect path.
*/
bool InterfaceDesignerLib::InstallFont( const char* fontPath) {
	return Font::InstallFont( fontPath);
}

/**
* Adds a new objects to the container.
*
* @param id		:: The unique ID of the new object (if the ID is already is use then the object
*					won't be added).
* @param name	:: The name of the object.
* @param type	:: The type of object.
* @param x		:: The x position of the object.
* @param y		:: The y position of the object.
*
* @return True if the object was added correctly.
*/
bool InterfaceDesignerLib::AddNewObject( s32& id, const char* name, s32 type, float x, float y) {
	id = mp_interface->GetAvailableUniqueID();

	// Early out if there are already too many objects.
	if( id == -1)
		return true;

	InterfaceObject* obj = new InterfaceObject( id, InterfaceObjectType( type));
	obj->SetName( name);
	obj->MoveTo( x, y, 1.0f);
	obj->ScaleTo( 0.1f, 0.1f, 1.0f);
	obj->SetText( "Text");

	if( !mp_interface->AddNewObject( obj))
		SafeDelete( obj);

	m_objSelected = -1;

	return true;
}