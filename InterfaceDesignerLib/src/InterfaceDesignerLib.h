/********************************************************************
*
*	CLASS		:: InterfaceDesignerLib
*	DESCRIPTION	:: Used by the tool to render the DirectX panel.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2015 / 07 / 24
*
********************************************************************/

#ifndef InterfaceDesignerLibH
#define InterfaceDesignerLibH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include <DBEApp.h>
#include <DBEColour.h>
#include <DBEMath.h>
#include <DBERandom.h>

namespace DBE {
	class Camera;
	class Font;
	class UITexture;
}
class InterfaceContainer;
class InterfaceObject;
/********************************************************************
*	Defines and constants.
********************************************************************/
enum CursorType {
	CT_None = 0,
	CT_TopLeft,
	CT_TopRight,
	CT_Top,
	CT_BottomLeft,
	CT_BottomRight,
	CT_Bottom,
	CT_Left,
	CT_Right,
	CT_Middle,
	CT_Count,
};


/*******************************************************************/
class InterfaceDesignerLib : public DBE::App {
	public:
		/// Constructor.
		InterfaceDesignerLib() {}

		bool HandleInit();
		bool HandleInput() { return true; }		// Not needed inside a tool.
		bool HandleUpdate( float deltaTime);
		bool HandleRender( float deltaTime);
		bool HandleShutdown();

		/// Saves the current interface to the specified file.
		bool SaveInterface( const char* file) const;
		/// Loads an interface from the specified file.
		bool LoadInterface( const char* file);

		/// Gets the current number of objects in the scene.
		bool GetObjectCount( s32& count);
		/// Gets the object at the specified index.
		bool GetObjectAtIndex( s32 index, s32& id, const char* &name);

		bool AddObjectButton( s32& id, const char* name, float x, float y);
		bool AddObjectIcon( s32& id, const char* name, float x, float y);
		bool AddObjectText( s32& id, const char* name, float x, float y);
		/// Deletes an object from the interface.
		bool DeleteObject( s32 id);
		/// Deletes all objects from the interface.
		bool DeleteAllObjects();

		/// Iterates through the names of events that objects have.
		bool GetObjectEventNames( s32 index, const char* &eventName, s32& eventCount);
		/// Gets the maximum string length permitted for Lua event scripts.
		bool GetObjectEventStringLimit( s32& limit);

		/// Gets an object's name from its ID.
		bool GetObjectNameFromID( s32 id, const char* &name);
		/// Gets an object's type (in string form) from its ID.
		bool GetObjectTypeFromID( s32 id, char* type);
		/// Gets an object's location from its ID.
		bool GetObjectLocationFromID( s32 id, float& x, float& y, float& z);
		/// Gets an object's dimensions from its ID.
		bool GetObjectSizeFromID( s32 id, float& width, float& height);
		/// Gets an object's texture path from its ID.
		bool GetObjectTexturePathFromID( s32 id, const char* &texturePath);
		/// Gets an object's text.
		bool GetObjectTextFromID( s32 id, const char* &text);
		/// Gets an object's font's colour from its ID.
		bool GetObjectFontColourFromID( int id, s32& red, s32& green, s32& blue, s32& alpha);
		/// Gets an object's event's Lua script.
		bool GetObjectEventScriptFromID( s32 id, const char* eventName, char* script);

		/// Sets an object's name from its ID.
		bool SetObjectNameFromID( s32 id, const char* name);
		/// Sets an object's location from its ID.
		bool SetObjectLocationFromID( s32 id, float x, float y, float z);
		/// Sets an object's dimensions from its ID.
		bool SetObjectSizeFromID( s32 id, float width, float height);
		/// Sets an object's texture path from its ID.
		bool SetObjectTexturePathFromID( s32 id, const char* texturePath);
		/// Sets an object's text.
		bool SetObjectTextFromID( s32 id, const char* text);
		/// Sets an object's font's colour from its ID.
		bool SetObjectFontColourFromID( s32 id, s32 red, s32 green, s32 blue, s32 alpha);
		/// Sets an object's event's Lua script.
		bool SetObjectEventScriptFromID( s32 id, const char* eventName, char* script);

		/// Check if the mouse cursor is over an object.
		bool TestCursorIsOverObject( float x, float y, s32& id);
		/// Gets what type of cursor should be displayed.
		bool TestCursorPosOverObject( float x, float y, s32& type);
		/// Gets the currently selected object.
		bool GetSelectedObject( s32& id);
		/// Sets the currently selected object.
		bool SetSelectedObject( s32 id);

		/// Moves an object using its ID.
		bool MoveObjectBy( s32 id, float x, float y);
		/// Scales an object using its ID.
		bool ScaleObjectBy( s32 id, float x, float y, s32 dir);

		/// Installs a font.
		bool InstallFont( const char* fontPath);

	private:
		bool AddNewObject( s32& id, const char* name, s32 type, float x, float y);

		InterfaceContainer*	mp_interface;

		DBE::UITexture*	mp_selection;
		s32				m_objSelected;

		/// Private copy constructor to prevent multiple instances.
		InterfaceDesignerLib( const InterfaceDesignerLib&);
		/// Private assignment operator to prevent multiple instances.
		InterfaceDesignerLib& operator=( const InterfaceDesignerLib&);

};

/*******************************************************************/
#endif	// #ifndef InterfaceDesignerLibH