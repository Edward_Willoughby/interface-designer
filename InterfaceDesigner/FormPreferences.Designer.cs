﻿namespace InterfaceDesigner
{
	partial class FormPreferences
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.lblResolution = new System.Windows.Forms.Label();
			this.comboResolution = new System.Windows.Forms.ComboBox();
			this.cbFullScreen = new System.Windows.Forms.CheckBox();
			this.btnConfirm = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// lblResolution
			// 
			this.lblResolution.AutoSize = true;
			this.lblResolution.Location = new System.Drawing.Point(13, 13);
			this.lblResolution.Name = "lblResolution";
			this.lblResolution.Size = new System.Drawing.Size(60, 13);
			this.lblResolution.TabIndex = 0;
			this.lblResolution.Text = "Resolution:";
			// 
			// comboResolution
			// 
			this.comboResolution.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboResolution.FormattingEnabled = true;
			this.comboResolution.Location = new System.Drawing.Point(83, 10);
			this.comboResolution.Name = "comboResolution";
			this.comboResolution.Size = new System.Drawing.Size(155, 21);
			this.comboResolution.TabIndex = 1;
			// 
			// cbFullScreen
			// 
			this.cbFullScreen.AutoSize = true;
			this.cbFullScreen.Location = new System.Drawing.Point(16, 53);
			this.cbFullScreen.Name = "cbFullScreen";
			this.cbFullScreen.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
			this.cbFullScreen.Size = new System.Drawing.Size(85, 17);
			this.cbFullScreen.TabIndex = 4;
			this.cbFullScreen.Text = "?Full Screen";
			this.cbFullScreen.UseVisualStyleBackColor = true;
			// 
			// btnConfirm
			// 
			this.btnConfirm.Location = new System.Drawing.Point(41, 92);
			this.btnConfirm.Name = "btnConfirm";
			this.btnConfirm.Size = new System.Drawing.Size(75, 23);
			this.btnConfirm.TabIndex = 5;
			this.btnConfirm.Text = "Confirm";
			this.btnConfirm.UseVisualStyleBackColor = true;
			this.btnConfirm.Click += new System.EventHandler(this.btnConfirm_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.Location = new System.Drawing.Point(163, 92);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(75, 23);
			this.btnCancel.TabIndex = 6;
			this.btnCancel.Text = "Cancel";
			this.btnCancel.UseVisualStyleBackColor = true;
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// FormPreferences
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(284, 127);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.btnConfirm);
			this.Controls.Add(this.cbFullScreen);
			this.Controls.Add(this.comboResolution);
			this.Controls.Add(this.lblResolution);
			this.Name = "FormPreferences";
			this.Text = "Preferences";
			this.Shown += new System.EventHandler(this.FormPreferences_Shown);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label lblResolution;
		private System.Windows.Forms.ComboBox comboResolution;
		private System.Windows.Forms.CheckBox cbFullScreen;
		private System.Windows.Forms.Button btnConfirm;
		private System.Windows.Forms.Button btnCancel;


	}
}