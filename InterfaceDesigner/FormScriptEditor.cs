﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InterfaceDesigner
{
	public partial class FormScriptEditor : Form
	{
		static public Point formLocation;
		static public Size formSize;
		static public bool firstTime = true;

		public string script = "";

		private string oldScript;
		private bool save;

		public FormScriptEditor( string name, string s)
		{
			InitializeComponent();

			if (firstTime)
			{
				Rectangle rect = Screen.PrimaryScreen.Bounds;
				Point pos = new Point((rect.Width / 4) - (this.Width / 2), (rect.Height / 3) - (this.Height / 2));
				formLocation = pos;

				formSize = this.Size;

				firstTime = false;
			}

			this.Text += " - " + name;

			tbScript.MaxLength = FormMain.EventStringLimit;

			script = s;
			tbScript.Text = script;
			tbScript_TextChanged(tbScript, null);

			oldScript = script;
			save = false;
		}

		private void FormScriptEditor_Shown(object sender, EventArgs e)
		{
			this.Location = formLocation;
			this.Size = formSize;

			FormScriptEditor_Resize(sender, e);
		}

		private void FormScriptEditor_Resize(object sender, EventArgs e)
		{
			cbScripts.Width = (this.Width - 16) - (24 + btnScriptManager.Width + 6);

			btnScriptManager.Location = new Point(cbScripts.Location.X + cbScripts.Width + 6, btnScriptManager.Location.Y);

			tbScript.Width = (this.Width - 16) - 24;
			tbScript.Height = (this.Height - 50) - (12 + 45 + cbScripts.Height + 6);

			lblCharCount.Location = new Point(lblCharCount.Location.X, tbScript.Location.Y + tbScript.Height + 3);

			btnConfirm.Location = new Point((this.Width / 2) - (8 + btnConfirm.Width), (this.Height - 50) - btnConfirm.Height);
			btnCancel.Location = new Point((this.Width / 2) + 8, (this.Height - 50) - btnCancel.Height);
		}

		private void FormScriptEditor_FormClosing(object sender, FormClosingEventArgs e)
		{
			if (save)
				script = tbScript.Text;

			formLocation = this.Location;
			formSize = this.Size;
		}

		private void btnConfirm_Click(object sender, EventArgs e)
		{
			save = true;
			this.Close();
		}

		private void btnCancel_Click(object sender, EventArgs e)
		{
			save = false;
			this.Close();
		}

		private void tbScript_TextChanged(object sender, EventArgs e)
		{
			lblCharCount.Text = "Character Count: " + tbScript.Text.Length + " / " + tbScript.MaxLength;
		}
	}
}
