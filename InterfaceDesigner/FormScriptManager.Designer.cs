﻿namespace InterfaceDesigner
{
	partial class FormScriptManager
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.listScripts = new System.Windows.Forms.ListBox();
			this.tbScriptName = new System.Windows.Forms.TextBox();
			this.btnAddScript = new System.Windows.Forms.Button();
			this.btnDeleteScript = new System.Windows.Forms.Button();
			this.tbScript = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// listScripts
			// 
			this.listScripts.FormattingEnabled = true;
			this.listScripts.HorizontalScrollbar = true;
			this.listScripts.Location = new System.Drawing.Point(12, 102);
			this.listScripts.Name = "listScripts";
			this.listScripts.Size = new System.Drawing.Size(182, 121);
			this.listScripts.TabIndex = 0;
			this.listScripts.SelectedIndexChanged += new System.EventHandler(this.listScripts_SelectedIndexChanged);
			// 
			// tbScriptName
			// 
			this.tbScriptName.Location = new System.Drawing.Point(13, 76);
			this.tbScriptName.Name = "tbScriptName";
			this.tbScriptName.Size = new System.Drawing.Size(100, 20);
			this.tbScriptName.TabIndex = 1;
			// 
			// btnAddScript
			// 
			this.btnAddScript.Location = new System.Drawing.Point(119, 74);
			this.btnAddScript.Name = "btnAddScript";
			this.btnAddScript.Size = new System.Drawing.Size(75, 23);
			this.btnAddScript.TabIndex = 2;
			this.btnAddScript.Text = "Add Script";
			this.btnAddScript.UseVisualStyleBackColor = true;
			this.btnAddScript.Click += new System.EventHandler(this.btnAddScript_Click);
			// 
			// btnDeleteScript
			// 
			this.btnDeleteScript.Location = new System.Drawing.Point(13, 230);
			this.btnDeleteScript.Name = "btnDeleteScript";
			this.btnDeleteScript.Size = new System.Drawing.Size(181, 23);
			this.btnDeleteScript.TabIndex = 3;
			this.btnDeleteScript.Text = "Delete Script";
			this.btnDeleteScript.UseVisualStyleBackColor = true;
			this.btnDeleteScript.Click += new System.EventHandler(this.btnDeleteScript_Click);
			// 
			// tbScript
			// 
			this.tbScript.Location = new System.Drawing.Point(201, 102);
			this.tbScript.Multiline = true;
			this.tbScript.Name = "tbScript";
			this.tbScript.ScrollBars = System.Windows.Forms.ScrollBars.Both;
			this.tbScript.Size = new System.Drawing.Size(185, 121);
			this.tbScript.TabIndex = 4;
			// 
			// FormScriptManager
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(467, 337);
			this.Controls.Add(this.tbScript);
			this.Controls.Add(this.btnDeleteScript);
			this.Controls.Add(this.btnAddScript);
			this.Controls.Add(this.tbScriptName);
			this.Controls.Add(this.listScripts);
			this.Name = "FormScriptManager";
			this.Text = "FormScriptManager";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.ListBox listScripts;
		private System.Windows.Forms.TextBox tbScriptName;
		private System.Windows.Forms.Button btnAddScript;
		private System.Windows.Forms.Button btnDeleteScript;
		private System.Windows.Forms.TextBox tbScript;
	}
}