﻿namespace InterfaceDesigner
{
	partial class FormScriptEditor
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.tbScript = new System.Windows.Forms.TextBox();
			this.btnConfirm = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.lblCharCount = new System.Windows.Forms.Label();
			this.cbScripts = new System.Windows.Forms.ComboBox();
			this.btnScriptManager = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// tbScript
			// 
			this.tbScript.Location = new System.Drawing.Point(12, 39);
			this.tbScript.Multiline = true;
			this.tbScript.Name = "tbScript";
			this.tbScript.ScrollBars = System.Windows.Forms.ScrollBars.Both;
			this.tbScript.Size = new System.Drawing.Size(460, 193);
			this.tbScript.TabIndex = 0;
			this.tbScript.TextChanged += new System.EventHandler(this.tbScript_TextChanged);
			// 
			// btnConfirm
			// 
			this.btnConfirm.Location = new System.Drawing.Point(12, 254);
			this.btnConfirm.Name = "btnConfirm";
			this.btnConfirm.Size = new System.Drawing.Size(126, 23);
			this.btnConfirm.TabIndex = 1;
			this.btnConfirm.Text = "Confirm";
			this.btnConfirm.UseVisualStyleBackColor = true;
			this.btnConfirm.Click += new System.EventHandler(this.btnConfirm_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.Location = new System.Drawing.Point(154, 254);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(118, 23);
			this.btnCancel.TabIndex = 2;
			this.btnCancel.Text = "Cancel";
			this.btnCancel.UseVisualStyleBackColor = true;
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// lblCharCount
			// 
			this.lblCharCount.AutoSize = true;
			this.lblCharCount.Location = new System.Drawing.Point(12, 235);
			this.lblCharCount.Name = "lblCharCount";
			this.lblCharCount.Size = new System.Drawing.Size(58, 13);
			this.lblCharCount.TabIndex = 3;
			this.lblCharCount.Text = "Characters";
			// 
			// cbScripts
			// 
			this.cbScripts.FormattingEnabled = true;
			this.cbScripts.Location = new System.Drawing.Point(12, 12);
			this.cbScripts.Name = "cbScripts";
			this.cbScripts.Size = new System.Drawing.Size(231, 21);
			this.cbScripts.TabIndex = 4;
			// 
			// btnScriptManager
			// 
			this.btnScriptManager.Location = new System.Drawing.Point(449, 10);
			this.btnScriptManager.Name = "btnScriptManager";
			this.btnScriptManager.Size = new System.Drawing.Size(23, 23);
			this.btnScriptManager.TabIndex = 5;
			this.btnScriptManager.Text = "...";
			this.btnScriptManager.UseVisualStyleBackColor = true;
			// 
			// FormScriptEditor
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(484, 462);
			this.Controls.Add(this.btnScriptManager);
			this.Controls.Add(this.cbScripts);
			this.Controls.Add(this.lblCharCount);
			this.Controls.Add(this.tbScript);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.btnConfirm);
			this.MinimumSize = new System.Drawing.Size(500, 500);
			this.Name = "FormScriptEditor";
			this.Text = "Script Editor";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormScriptEditor_FormClosing);
			this.Shown += new System.EventHandler(this.FormScriptEditor_Shown);
			this.Resize += new System.EventHandler(this.FormScriptEditor_Resize);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TextBox tbScript;
		private System.Windows.Forms.Button btnConfirm;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Label lblCharCount;
		private System.Windows.Forms.ComboBox cbScripts;
		private System.Windows.Forms.Button btnScriptManager;
	}
}