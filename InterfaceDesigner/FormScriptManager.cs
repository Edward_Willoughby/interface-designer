﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InterfaceDesigner
{
	public partial class FormScriptManager : Form
	{
		/// <summary>
		/// Comparer for the list of objects so that they can be sorted by value instead of by key.
		/// </summary>
		static int CompareObjectNames(KeyValuePair<int, string> a, KeyValuePair<int, string> b)
		{
			return a.Value.CompareTo(b.Value);
		}


		static public SortedDictionary<string, string> scripts = new SortedDictionary<string, string>();
				

		public FormScriptManager()
		{
			InitializeComponent();

			this.AddScript("firstScript", "Do this thing");
			this.AddScript("secondScript", "Do another");

			RepopulateScriptList();
		}

		public void AddScript(string key, string value)
		{
			if (!scripts.ContainsKey(key))
				scripts.Add(key, value);
		}

		public void DeleteScript(string key)
		{
			if (scripts.ContainsKey(key))
				scripts.Remove(key);
		}

		private void btnAddScript_Click(object sender, EventArgs e)
		{
			string name = tbScriptName.Text.Trim();

			if (scriptNameIsValid(name))
			{
				this.AddScript(name, "");
				RepopulateScriptList();

				tbScriptName.Text = "";
			}
		}

		private void listScripts_SelectedIndexChanged(object sender, EventArgs e)
		{
			string key = (string)listScripts.SelectedItem;

			foreach (KeyValuePair<string, string> item in scripts)
			{
				if (item.Key == key)
				{
					tbScript.Text = item.Value;
					break;
				}
			}
		}

		private void btnDeleteScript_Click(object sender, EventArgs e)
		{
			string key = (string)listScripts.SelectedItem;

			if (scripts.ContainsKey(key))
			{
				this.DeleteScript(key);
				RepopulateScriptList();
				tbScript.Text = "";
			}
		}

		private void RepopulateScriptList()
		{
			listScripts.Items.Clear();

			foreach (KeyValuePair<string, string> item in scripts)
			{
				listScripts.Items.Add(item.Key);
			}
		}

		private bool scriptNameIsValid(string name)
		{
			if (name == "" || name == null)
			{
				MessageBox.Show("Invalid name for script.", "Script Naming Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return false;
			}

			bool keyExists = false;
			foreach (KeyValuePair<string, string> item in scripts)
			{
				if (name.ToUpper() == item.Key.ToUpper())
				{
					keyExists = true;
					break;
				}
			}
			if (keyExists)
			{
				MessageBox.Show("Script name already exists.", "Script Naming Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return false;
			}

			return true;
		}
	}
}
