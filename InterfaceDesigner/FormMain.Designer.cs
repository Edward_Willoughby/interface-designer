﻿namespace InterfaceDesigner
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
			this.timerDirectXRefresh = new System.Windows.Forms.Timer(this.components);
			this.containerAll = new System.Windows.Forms.SplitContainer();
			this.macroBar = new System.Windows.Forms.ToolStrip();
			this.macroAddIcon = new System.Windows.Forms.ToolStripButton();
			this.macroAddText = new System.Windows.Forms.ToolStripButton();
			this.macroAddButton = new System.Windows.Forms.ToolStripButton();
			this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
			this.macroRun = new System.Windows.Forms.ToolStripButton();
			this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
			this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
			this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
			this.menuBar = new System.Windows.Forms.MenuStrip();
			this.menuBarItemFile = new System.Windows.Forms.ToolStripMenuItem();
			this.menuBarItemFileNew = new System.Windows.Forms.ToolStripMenuItem();
			this.menuBarItemFileOpen = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
			this.menuBarItemFileSave = new System.Windows.Forms.ToolStripMenuItem();
			this.menuBarItemFileSaveAs = new System.Windows.Forms.ToolStripMenuItem();
			this.menuBarItemScriptManager = new System.Windows.Forms.ToolStripMenuItem();
			this.menuBarItemPreferences = new System.Windows.Forms.ToolStripMenuItem();
			this.menuBarItemAbout = new System.Windows.Forms.ToolStripMenuItem();
			this.containerLeftRight = new System.Windows.Forms.SplitContainer();
			this.panelRender = new System.Windows.Forms.Panel();
			this.containerObjects = new System.Windows.Forms.SplitContainer();
			this.panelSceneGraph = new System.Windows.Forms.Panel();
			this.lblTitleSceneGraph = new System.Windows.Forms.Label();
			this.treeSceneGraph = new System.Windows.Forms.TreeView();
			this.panelObjectProps = new System.Windows.Forms.Panel();
			this.propsObjectProps = new System.Windows.Forms.PropertyGrid();
			this.lblTitleObjectProps = new System.Windows.Forms.Label();
			this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
			this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
			this.openFontDialog = new System.Windows.Forms.OpenFileDialog();
			((System.ComponentModel.ISupportInitialize)(this.containerAll)).BeginInit();
			this.containerAll.Panel1.SuspendLayout();
			this.containerAll.Panel2.SuspendLayout();
			this.containerAll.SuspendLayout();
			this.macroBar.SuspendLayout();
			this.menuBar.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.containerLeftRight)).BeginInit();
			this.containerLeftRight.Panel1.SuspendLayout();
			this.containerLeftRight.Panel2.SuspendLayout();
			this.containerLeftRight.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.containerObjects)).BeginInit();
			this.containerObjects.Panel1.SuspendLayout();
			this.containerObjects.Panel2.SuspendLayout();
			this.containerObjects.SuspendLayout();
			this.panelSceneGraph.SuspendLayout();
			this.panelObjectProps.SuspendLayout();
			this.SuspendLayout();
			// 
			// timerDirectXRefresh
			// 
			this.timerDirectXRefresh.Interval = 10;
			this.timerDirectXRefresh.Tick += new System.EventHandler(this.timerDirectXRefresh_Tick);
			// 
			// containerAll
			// 
			this.containerAll.Dock = System.Windows.Forms.DockStyle.Fill;
			this.containerAll.Location = new System.Drawing.Point(0, 0);
			this.containerAll.Name = "containerAll";
			this.containerAll.Orientation = System.Windows.Forms.Orientation.Horizontal;
			// 
			// containerAll.Panel1
			// 
			this.containerAll.Panel1.Controls.Add(this.macroBar);
			this.containerAll.Panel1.Controls.Add(this.menuBar);
			// 
			// containerAll.Panel2
			// 
			this.containerAll.Panel2.Controls.Add(this.containerLeftRight);
			this.containerAll.Size = new System.Drawing.Size(856, 515);
			this.containerAll.SplitterDistance = 84;
			this.containerAll.TabIndex = 0;
			// 
			// macroBar
			// 
			this.macroBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.macroAddIcon,
            this.macroAddText,
            this.macroAddButton,
            this.toolStripSeparator3,
            this.macroRun,
            this.toolStripSeparator2,
            this.toolStripButton1,
            this.toolStripLabel1});
			this.macroBar.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
			this.macroBar.Location = new System.Drawing.Point(0, 24);
			this.macroBar.Name = "macroBar";
			this.macroBar.Size = new System.Drawing.Size(856, 25);
			this.macroBar.TabIndex = 1;
			this.macroBar.Text = "macroBar";
			// 
			// macroAddIcon
			// 
			this.macroAddIcon.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.macroAddIcon.Image = ((System.Drawing.Image)(resources.GetObject("macroAddIcon.Image")));
			this.macroAddIcon.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.macroAddIcon.Name = "macroAddIcon";
			this.macroAddIcon.Size = new System.Drawing.Size(23, 22);
			this.macroAddIcon.Text = "Add Icon";
			this.macroAddIcon.Click += new System.EventHandler(this.macroButton_Click);
			// 
			// macroAddText
			// 
			this.macroAddText.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.macroAddText.Image = ((System.Drawing.Image)(resources.GetObject("macroAddText.Image")));
			this.macroAddText.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.macroAddText.Name = "macroAddText";
			this.macroAddText.Size = new System.Drawing.Size(23, 22);
			this.macroAddText.Text = "Add Text";
			this.macroAddText.Click += new System.EventHandler(this.macroButton_Click);
			// 
			// macroAddButton
			// 
			this.macroAddButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.macroAddButton.Image = ((System.Drawing.Image)(resources.GetObject("macroAddButton.Image")));
			this.macroAddButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.macroAddButton.Name = "macroAddButton";
			this.macroAddButton.Size = new System.Drawing.Size(23, 22);
			this.macroAddButton.Text = "Add Button";
			this.macroAddButton.Click += new System.EventHandler(this.macroButton_Click);
			// 
			// toolStripSeparator3
			// 
			this.toolStripSeparator3.Name = "toolStripSeparator3";
			this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
			// 
			// macroRun
			// 
			this.macroRun.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.macroRun.Image = ((System.Drawing.Image)(resources.GetObject("macroRun.Image")));
			this.macroRun.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.macroRun.Name = "macroRun";
			this.macroRun.Size = new System.Drawing.Size(23, 22);
			this.macroRun.Text = "(F5) Run";
			this.macroRun.Click += new System.EventHandler(this.macroButton_Click);
			// 
			// toolStripSeparator2
			// 
			this.toolStripSeparator2.Name = "toolStripSeparator2";
			this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
			// 
			// toolStripButton1
			// 
			this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
			this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.toolStripButton1.Name = "toolStripButton1";
			this.toolStripButton1.Size = new System.Drawing.Size(23, 22);
			this.toolStripButton1.Text = "toolStripButton1";
			this.toolStripButton1.Click += new System.EventHandler(this.macroButton_Click);
			// 
			// toolStripLabel1
			// 
			this.toolStripLabel1.Name = "toolStripLabel1";
			this.toolStripLabel1.Size = new System.Drawing.Size(71, 22);
			this.toolStripLabel1.Text = "Cursor Type";
			// 
			// menuBar
			// 
			this.menuBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuBarItemFile,
            this.menuBarItemScriptManager,
            this.menuBarItemPreferences,
            this.menuBarItemAbout});
			this.menuBar.Location = new System.Drawing.Point(0, 0);
			this.menuBar.Name = "menuBar";
			this.menuBar.Size = new System.Drawing.Size(856, 24);
			this.menuBar.TabIndex = 0;
			this.menuBar.Text = "menuBar";
			// 
			// menuBarItemFile
			// 
			this.menuBarItemFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuBarItemFileNew,
            this.menuBarItemFileOpen,
            this.toolStripSeparator1,
            this.menuBarItemFileSave,
            this.menuBarItemFileSaveAs});
			this.menuBarItemFile.Name = "menuBarItemFile";
			this.menuBarItemFile.Size = new System.Drawing.Size(37, 20);
			this.menuBarItemFile.Text = "File";
			// 
			// menuBarItemFileNew
			// 
			this.menuBarItemFileNew.Name = "menuBarItemFileNew";
			this.menuBarItemFileNew.ShortcutKeyDisplayString = "Ctrl+N";
			this.menuBarItemFileNew.Size = new System.Drawing.Size(195, 22);
			this.menuBarItemFileNew.Text = "&New";
			this.menuBarItemFileNew.Click += new System.EventHandler(this.menuBarItemFileNew_Click);
			// 
			// menuBarItemFileOpen
			// 
			this.menuBarItemFileOpen.Name = "menuBarItemFileOpen";
			this.menuBarItemFileOpen.ShortcutKeyDisplayString = "Ctrl+O";
			this.menuBarItemFileOpen.Size = new System.Drawing.Size(195, 22);
			this.menuBarItemFileOpen.Text = "&Open";
			this.menuBarItemFileOpen.Click += new System.EventHandler(this.menuBarItemFileOpen_Click);
			// 
			// toolStripSeparator1
			// 
			this.toolStripSeparator1.Name = "toolStripSeparator1";
			this.toolStripSeparator1.Size = new System.Drawing.Size(192, 6);
			// 
			// menuBarItemFileSave
			// 
			this.menuBarItemFileSave.Name = "menuBarItemFileSave";
			this.menuBarItemFileSave.ShortcutKeyDisplayString = "Ctrl+S";
			this.menuBarItemFileSave.Size = new System.Drawing.Size(195, 22);
			this.menuBarItemFileSave.Text = "&Save";
			this.menuBarItemFileSave.Click += new System.EventHandler(this.menuBarItemFileSave_Click);
			// 
			// menuBarItemFileSaveAs
			// 
			this.menuBarItemFileSaveAs.Name = "menuBarItemFileSaveAs";
			this.menuBarItemFileSaveAs.ShortcutKeyDisplayString = "Ctrl+Shift+S";
			this.menuBarItemFileSaveAs.Size = new System.Drawing.Size(195, 22);
			this.menuBarItemFileSaveAs.Text = "&Save As...";
			this.menuBarItemFileSaveAs.Click += new System.EventHandler(this.menuBarItemFileSaveAs_Click);
			// 
			// menuBarItemScriptManager
			// 
			this.menuBarItemScriptManager.Name = "menuBarItemScriptManager";
			this.menuBarItemScriptManager.Size = new System.Drawing.Size(99, 20);
			this.menuBarItemScriptManager.Text = "Script Manager";
			this.menuBarItemScriptManager.Click += new System.EventHandler(this.menuBarItemScriptManager_Click);
			// 
			// menuBarItemPreferences
			// 
			this.menuBarItemPreferences.Name = "menuBarItemPreferences";
			this.menuBarItemPreferences.Size = new System.Drawing.Size(80, 20);
			this.menuBarItemPreferences.Text = "Preferences";
			this.menuBarItemPreferences.Click += new System.EventHandler(this.menuBarItemPreferences_Click);
			// 
			// menuBarItemAbout
			// 
			this.menuBarItemAbout.Name = "menuBarItemAbout";
			this.menuBarItemAbout.Size = new System.Drawing.Size(52, 20);
			this.menuBarItemAbout.Text = "About";
			this.menuBarItemAbout.Click += new System.EventHandler(this.menuBarItemAbout_Click);
			// 
			// containerLeftRight
			// 
			this.containerLeftRight.Dock = System.Windows.Forms.DockStyle.Fill;
			this.containerLeftRight.Location = new System.Drawing.Point(0, 0);
			this.containerLeftRight.Name = "containerLeftRight";
			// 
			// containerLeftRight.Panel1
			// 
			this.containerLeftRight.Panel1.Controls.Add(this.panelRender);
			// 
			// containerLeftRight.Panel2
			// 
			this.containerLeftRight.Panel2.Controls.Add(this.containerObjects);
			this.containerLeftRight.Size = new System.Drawing.Size(856, 427);
			this.containerLeftRight.SplitterDistance = 570;
			this.containerLeftRight.TabIndex = 0;
			// 
			// panelRender
			// 
			this.panelRender.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelRender.Location = new System.Drawing.Point(0, 0);
			this.panelRender.Name = "panelRender";
			this.panelRender.Size = new System.Drawing.Size(570, 427);
			this.panelRender.TabIndex = 0;
			this.panelRender.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panelRender_MouseDown);
			this.panelRender.MouseEnter += new System.EventHandler(this.panelRender_MouseEnter);
			this.panelRender.MouseHover += new System.EventHandler(this.panelRender_MouseHover);
			this.panelRender.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panelRender_MouseMove);
			this.panelRender.MouseUp += new System.Windows.Forms.MouseEventHandler(this.panelRender_MouseUp);
			// 
			// containerObjects
			// 
			this.containerObjects.Dock = System.Windows.Forms.DockStyle.Fill;
			this.containerObjects.Location = new System.Drawing.Point(0, 0);
			this.containerObjects.Name = "containerObjects";
			this.containerObjects.Orientation = System.Windows.Forms.Orientation.Horizontal;
			// 
			// containerObjects.Panel1
			// 
			this.containerObjects.Panel1.Controls.Add(this.panelSceneGraph);
			// 
			// containerObjects.Panel2
			// 
			this.containerObjects.Panel2.Controls.Add(this.panelObjectProps);
			this.containerObjects.Size = new System.Drawing.Size(282, 427);
			this.containerObjects.SplitterDistance = 193;
			this.containerObjects.TabIndex = 0;
			this.containerObjects.SplitterMoved += new System.Windows.Forms.SplitterEventHandler(this.containerObjects_SplitterMoved);
			// 
			// panelSceneGraph
			// 
			this.panelSceneGraph.Controls.Add(this.lblTitleSceneGraph);
			this.panelSceneGraph.Controls.Add(this.treeSceneGraph);
			this.panelSceneGraph.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelSceneGraph.Location = new System.Drawing.Point(0, 0);
			this.panelSceneGraph.Name = "panelSceneGraph";
			this.panelSceneGraph.Size = new System.Drawing.Size(282, 193);
			this.panelSceneGraph.TabIndex = 0;
			// 
			// lblTitleSceneGraph
			// 
			this.lblTitleSceneGraph.AutoSize = true;
			this.lblTitleSceneGraph.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblTitleSceneGraph.Location = new System.Drawing.Point(4, 4);
			this.lblTitleSceneGraph.Name = "lblTitleSceneGraph";
			this.lblTitleSceneGraph.Size = new System.Drawing.Size(90, 15);
			this.lblTitleSceneGraph.TabIndex = 1;
			this.lblTitleSceneGraph.Text = "Scene Graph";
			// 
			// treeSceneGraph
			// 
			this.treeSceneGraph.Location = new System.Drawing.Point(0, 22);
			this.treeSceneGraph.Name = "treeSceneGraph";
			this.treeSceneGraph.Size = new System.Drawing.Size(282, 171);
			this.treeSceneGraph.TabIndex = 0;
			this.treeSceneGraph.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.treeSceneGraph_NodeMouseClick);
			// 
			// panelObjectProps
			// 
			this.panelObjectProps.Controls.Add(this.propsObjectProps);
			this.panelObjectProps.Controls.Add(this.lblTitleObjectProps);
			this.panelObjectProps.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelObjectProps.Location = new System.Drawing.Point(0, 0);
			this.panelObjectProps.Name = "panelObjectProps";
			this.panelObjectProps.Size = new System.Drawing.Size(282, 230);
			this.panelObjectProps.TabIndex = 0;
			// 
			// propsObjectProps
			// 
			this.propsObjectProps.Location = new System.Drawing.Point(0, 22);
			this.propsObjectProps.Name = "propsObjectProps";
			this.propsObjectProps.Size = new System.Drawing.Size(282, 208);
			this.propsObjectProps.TabIndex = 1;
			this.propsObjectProps.PropertyValueChanged += new System.Windows.Forms.PropertyValueChangedEventHandler(this.propsObjectProps_PropertyValueChanged);
			// 
			// lblTitleObjectProps
			// 
			this.lblTitleObjectProps.AutoSize = true;
			this.lblTitleObjectProps.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblTitleObjectProps.Location = new System.Drawing.Point(3, 4);
			this.lblTitleObjectProps.Name = "lblTitleObjectProps";
			this.lblTitleObjectProps.Size = new System.Drawing.Size(118, 15);
			this.lblTitleObjectProps.TabIndex = 0;
			this.lblTitleObjectProps.Text = "Object Properties";
			// 
			// openFileDialog
			// 
			this.openFileDialog.FileName = "openFileDialog";
			this.openFileDialog.Filter = "Interface file|*.dbif";
			// 
			// saveFileDialog
			// 
			this.saveFileDialog.Filter = "Interface file|*.dbif";
			// 
			// openFontDialog
			// 
			this.openFontDialog.FileName = "openFontDialog";
			this.openFontDialog.Filter = "Font file|*ttf";
			// 
			// FormMain
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(856, 515);
			this.Controls.Add(this.containerAll);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
			this.KeyPreview = true;
			this.MainMenuStrip = this.menuBar;
			this.Name = "FormMain";
			this.Text = "Interface Designer";
			this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormMain_FormClosing);
			this.Shown += new System.EventHandler(this.FormMain_Shown);
			this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FormMain_KeyDown);
			this.containerAll.Panel1.ResumeLayout(false);
			this.containerAll.Panel1.PerformLayout();
			this.containerAll.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.containerAll)).EndInit();
			this.containerAll.ResumeLayout(false);
			this.macroBar.ResumeLayout(false);
			this.macroBar.PerformLayout();
			this.menuBar.ResumeLayout(false);
			this.menuBar.PerformLayout();
			this.containerLeftRight.Panel1.ResumeLayout(false);
			this.containerLeftRight.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.containerLeftRight)).EndInit();
			this.containerLeftRight.ResumeLayout(false);
			this.containerObjects.Panel1.ResumeLayout(false);
			this.containerObjects.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.containerObjects)).EndInit();
			this.containerObjects.ResumeLayout(false);
			this.panelSceneGraph.ResumeLayout(false);
			this.panelSceneGraph.PerformLayout();
			this.panelObjectProps.ResumeLayout(false);
			this.panelObjectProps.PerformLayout();
			this.ResumeLayout(false);

        }

        #endregion

		private System.Windows.Forms.Timer timerDirectXRefresh;
		private System.Windows.Forms.SplitContainer containerAll;
		private System.Windows.Forms.SplitContainer containerLeftRight;
		private System.Windows.Forms.Panel panelRender;
		private System.Windows.Forms.SplitContainer containerObjects;
		private System.Windows.Forms.MenuStrip menuBar;
		private System.Windows.Forms.ToolStripMenuItem menuBarItemFile;
		private System.Windows.Forms.ToolStripMenuItem menuBarItemFileNew;
		private System.Windows.Forms.ToolStripMenuItem menuBarItemFileSave;
		private System.Windows.Forms.ToolStripMenuItem menuBarItemFileSaveAs;
		private System.Windows.Forms.ToolStripMenuItem menuBarItemAbout;
		private System.Windows.Forms.ToolStripMenuItem menuBarItemFileOpen;
		private System.Windows.Forms.ToolStrip macroBar;
		private System.Windows.Forms.ToolStripButton macroAddIcon;
		private System.Windows.Forms.ToolStripButton macroAddButton;
		private System.Windows.Forms.ToolStripButton macroAddText;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
		private System.Windows.Forms.Panel panelSceneGraph;
		private System.Windows.Forms.Label lblTitleSceneGraph;
		private System.Windows.Forms.TreeView treeSceneGraph;
		private System.Windows.Forms.Panel panelObjectProps;
		private System.Windows.Forms.PropertyGrid propsObjectProps;
		private System.Windows.Forms.Label lblTitleObjectProps;
		private System.Windows.Forms.ToolStripButton toolStripButton1;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
		private System.Windows.Forms.OpenFileDialog openFileDialog;
		private System.Windows.Forms.SaveFileDialog saveFileDialog;
		private System.Windows.Forms.OpenFileDialog openFontDialog;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
		private System.Windows.Forms.ToolStripButton macroRun;
		private System.Windows.Forms.ToolStripLabel toolStripLabel1;
		private System.Windows.Forms.ToolStripMenuItem menuBarItemPreferences;
		private System.Windows.Forms.ToolStripMenuItem menuBarItemScriptManager;
    }
}

