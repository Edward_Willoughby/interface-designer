﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Design;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace InterfaceDesigner
{
	/// <summary>
	/// Used to display an object's position in the property grid.
	/// </summary>
	[TypeConverter(typeof(ExpandableObjectConverter))]
	public class Vector3
	{
		private float x;
		private float y;
		private float z;

		public float X
		{
			get { return x; }
			set { x = value; }
		}

		public float Y
		{
			get { return y; }
			set { y = value; }
		}

		public float Z
		{
			get { return z; }
			set { z = value; }
		}

		public override string ToString()
		{
			return x + ", " + y + ", " + z;
		}
	}

	/// <summary>
	/// Allows the user to select a file when editing the texture path, instead of manually typing
	/// the path.
	/// </summary>
	[Editor(typeof(FileSelectorTypeEditor), typeof(UITypeEditor))]
	public class FileSelectorTypeEditor : UITypeEditor
	{
		public string FolderTextures = "res\\textures\\";

		public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
		{
			if (context == null || context.Instance == null)
				return base.GetEditStyle(context);

			return UITypeEditorEditStyle.Modal;
		}

		public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value)
		{
			if (context == null || context.Instance == null || provider == null)
				return value;

			OpenFileDialog dlg = new OpenFileDialog();
			dlg.Filter = "DDS Files (*.dds)|*.dds";
			dlg.CheckFileExists = true;
			dlg.InitialDirectory = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase) + "\\" + FolderTextures;

			string filename = (string)value;
			if (!File.Exists(filename))
				filename = null;
			dlg.FileName = filename;

			using (dlg)
			{
				DialogResult res = dlg.ShowDialog();
				if (res == DialogResult.OK)
				{
					// Check the file is in the correct folder.
					string exePath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
					exePath += "\\" + FolderTextures;
					bool failed = false;

					if (dlg.FileName.Length < exePath.Length)
						failed = true;
					else if (dlg.FileName.Substring(0, exePath.Length) != exePath)
						failed = true;
					else
						filename = FolderTextures + System.IO.Path.GetFileName(dlg.FileName);

					if( failed)
						MessageBox.Show("The texture file must be in the folder '" + FolderTextures + "' in the tool's executable's directory.", "Invalid Texture File Location", MessageBoxButtons.OK, MessageBoxIcon.Error);
				}
			}

			return filename;
		}
	}

	/// <summary>
	/// Allows the user to select a colour from a colour picker.
	/// </summary>
	[Editor(typeof(ColourPickerTypeEditor), typeof(UITypeEditor))]
	public class ColourPickerTypeEditor : UITypeEditor
	{
		public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
		{
			if (context == null || context.Instance == null)
				return base.GetEditStyle(context);

			return UITypeEditorEditStyle.Modal;
		}

		public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value)
		{
			if (context == null || context.Instance == null || provider == null)
				return value;

			Color colour = (Color)value;
			ColorDialog dlg = new ColorDialog();
			dlg.Color = colour;
			dlg.FullOpen = true;
			dlg.AllowFullOpen = true;

			using (dlg)
			{
				DialogResult res = dlg.ShowDialog();
				if (res == DialogResult.OK)
					colour = dlg.Color;
			}

			return colour;
		}
	}

	/// <summary>
	/// Used to display multi-line text boxes in the property grid so the user has more freedom
	/// when entering their script.
	/// </summary>
	[Editor(typeof(EventScript), typeof(UITypeEditor))]
	[TypeConverter(typeof(ExpandableObjectConverter))]
	public class EventScript : UITypeEditor
	{
		public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
		{
			if (context == null || context.Instance == null)
				return base.GetEditStyle(context);

			return UITypeEditorEditStyle.Modal;
		}

		public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value)
		{
			if (context == null || context.Instance == null || provider == null)
				return value;

			string script = "";
			string eventName = ((ObjectPropertyDescriptor)context.PropertyDescriptor).m_property.m_name;

			using (FormScriptEditor f = new FormScriptEditor(eventName, (string)value))
			{
				f.ShowDialog();
				script = f.script;
			}

			return script;
		}
	}

	/// <summary>
	/// Stores the information about a single property in the property grid.
	/// </summary>
	public class ObjectProperty
	{
		public Attribute[] attributes;
		public string m_category;
		public string m_name;
		public string m_description;
		public string m_type;
		public object m_value;
		public bool m_readOnly;
		public bool m_visible;

		public ObjectProperty(string category, string name, string desc, Type type, object value, bool readOnly, bool visible)
		{
			m_category = category;
			m_name = name;
			m_description = desc;
			m_type = type.AssemblyQualifiedName;
			m_value = value;
			m_readOnly = readOnly;
			m_visible = visible;

			attributes = null;
		}

		public ObjectProperty(string category, string name, string desc, Type type, object value)
			: this(category, name, desc, type, value, false, true)
		{ }

		public ObjectProperty(string category, string name, string desc, Type type, object value, bool readOnly)
			: this(category, name, desc, type, value, readOnly, true)
		{ }
	}

	/// <summary>
	/// Describes an 'ObjectProperty' and is used to access the information in a way that the
	/// PropertyGrid can understand.
	/// </summary>
	public class ObjectPropertyDescriptor : PropertyDescriptor
	{
		public ObjectProperty m_property;

		public ObjectPropertyDescriptor(ref ObjectProperty prop, Attribute[] attrs)
			: base(prop.m_name, attrs)
		{
			m_property = prop;
		}

		/// <summary>
		/// The following function are simply overrides of the abstract base class
		/// PropertyDescriptor which shouldn't really need to be edited, unless ObjectProperty's
		/// functionality expands.
		/// </summary>
		#region PropertyDescriptor overrides
		public override Type ComponentType
		{
			get { return m_property.GetType(); }
		}

		public override Type PropertyType
		{
			get { return Type.GetType( m_property.m_type); }
		}

		public override bool IsReadOnly
		{
			get { return m_property.m_readOnly; }
		}

		public override bool CanResetValue(object component)
		{
			return false;
		}

		public override object GetValue(object component)
		{
			return m_property.m_value;
		}

		public override void ResetValue(object component)
		{
			// You can't reset it, so just don't try...
		}

		public override void SetValue(object component, object value)
		{
			m_property.m_value = value;
		}

		public override bool ShouldSerializeValue(object component)
		{
			return false;
		}
		#endregion
	}

	public class PropertyGridObject : CollectionBase, ICustomTypeDescriptor
	{
		public void Add(ObjectProperty value)
		{
			base.List.Add(value);
		}

		public void Remove(string name)
		{
			foreach (ObjectProperty prop in base.List)
			{
				if (prop.m_name == name)
				{
					base.List.Remove(prop);
					return;
				}
			}
		}

		public ObjectProperty GetProperty(string name)
		{
			return this[this.IndexOf(name)];
		}

		public int IndexOf(string name)
		{
			for (int i = 0; i < base.List.Count; ++i)
				if (((ObjectProperty)base.List[i]).m_name == name)
					return i;

			return -1;
		}

		public ObjectProperty this[int index]
		{
			get { return (ObjectProperty)base.List[index]; }
			set { base.List[index] = (ObjectProperty)value; }
		}

		public PropertyDescriptorCollection GetProperties(Attribute[] attrs)
		{
			PropertyDescriptor[] newProps = new PropertyDescriptor[this.Count];

			for (int i = 0; i < this.Count; ++i)
			{
				ObjectProperty prop = (ObjectProperty)this[i];
				ArrayList a = new ArrayList(attrs.ToArray());

				if (prop.m_category != null)
					a.Add(new CategoryAttribute(prop.m_category));
				if (prop.m_description != null)
					a.Add(new DescriptionAttribute(prop.m_description));
				//if (prop.m_editorTypeName != null)
				//	a.Add(new EditorAttribute(prop.m_editorTypeName, typeof(UITypeEditor)));
				//if (prop.m_converterTypeName != null)
				//	a.Add(new TypeConverterAttribute(prop.m_converterTypeName));

				if (prop.attributes != null)
					a.AddRange(prop.attributes);

				Attribute[] attributes = (Attribute[])a.ToArray(typeof(Attribute));

				newProps[i] = new ObjectPropertyDescriptor(ref prop, attributes);
			}

			return new PropertyDescriptorCollection(newProps);
		}

		/// <summary>
		/// The following functions are simply overrides of the abstract base class CollectionBase
		/// which shouldn't really need to be edited.
		/// </summary>
		#region CollectionBase overrides
		public String GetClassName()
		{
			return TypeDescriptor.GetClassName(this, true);
		}

		public AttributeCollection GetAttributes()
		{
			return TypeDescriptor.GetAttributes(this, true);
		}

		public String GetComponentName()
		{
			return TypeDescriptor.GetComponentName(this, true);
		}

		public TypeConverter GetConverter()
		{
			return TypeDescriptor.GetConverter(this, true);
		}

		public EventDescriptor GetDefaultEvent()
		{
			return TypeDescriptor.GetDefaultEvent(this, true);
		}

		public PropertyDescriptor GetDefaultProperty()
		{
			return TypeDescriptor.GetDefaultProperty(this, true);
		}

		public object GetEditor(Type editorBaseType)
		{
			return TypeDescriptor.GetEditor(this, editorBaseType, true);
		}

		public EventDescriptorCollection GetEvents(Attribute[] attributes)
		{
			return TypeDescriptor.GetEvents(this, attributes, true);
		}

		public EventDescriptorCollection GetEvents()
		{
			return TypeDescriptor.GetEvents(this, true);
		}

		public PropertyDescriptorCollection GetProperties()
		{
			return TypeDescriptor.GetProperties(this, true);
		}

		public object GetPropertyOwner(PropertyDescriptor pd)
		{
			return this;
		}
		#endregion
	}

	/// <summary>
	/// OBSOLETE
	/// Used for storing and displaying an object's properties in the property grid.
	/// </summary>
	//public class PropertyGridForObject
	//{
	//	private int id;
	//	private string name;
	//	private string type;

	//	private Vector3 location = new Vector3();
	//	private SizeF size;
	//	private Font font = new Font("Arial", 8, FontStyle.Regular);
	//	private string texture;

	//	private string scriptOnEnter;
	//	private string scriptOnHover;
	//	private string scriptOnClick;
	//	private string scriptOnExit;

	//	[ReadOnly(true)]
	//	[CategoryAttribute("Appearance")]
	//	[Description("Unique ID of the object.")]
	//	public int ID
	//	{
	//		get { return id; }
	//		set { id = value; }
	//	}

	//	[ReadOnly(false)]
	//	[CategoryAttribute("Appearance")]
	//	[Description("The name of the object.")]
	//	public string Name
	//	{
	//		get { return name; }
	//		set { name = value; }
	//	}

	//	[ReadOnly(true)]
	//	[CategoryAttribute("Appearance")]
	//	[Description("The type of the object.")]
	//	public string Type
	//	{
	//		get { return type; }
	//		set { type = value; }
	//	}

	//	[ReadOnly(false)]
	//	[CategoryAttribute("Appearance")]
	//	[Description("The position of the object.")]
	//	[TypeConverter(typeof(ExpandableObjectConverter))]
	//	public Vector3 Location
	//	{
	//		get { return location; }
	//		set { location = value; }
	//	}

	//	[ReadOnly(false)]
	//	[CategoryAttribute("Appearance")]
	//	[Description("The dimensions of the object's image.")]
	//	public SizeF Size
	//	{
	//		get { return size; }
	//		set { size = value; }
	//	}

	//	[ReadOnly(true)]
	//	[CategoryAttribute("Appearance")]
	//	[Description("The font used by the object's text.")]
	//	public Font Font
	//	{
	//		get { return font; }
	//		set { font = value; }
	//	}

	//	[ReadOnly(false)]
	//	[CategoryAttribute("Appearance")]
	//	[Description("The texture used for the object's image.")]
	//	[Editor(typeof(FileSelectorTypeEditor), typeof(UITypeEditor))]
	//	public string Texture
	//	{
	//		get { return texture; }
	//		set { texture = value; }
	//	}

	//	[ReadOnly(false)]
	//	[CategoryAttribute("Behaviour")]
	//	[Description("Lua script to run when the mouse goes over the object.")]
	//	public string OnEnter
	//	{
	//		get { return scriptOnEnter; }
	//		set { scriptOnEnter = value; }
	//	}

	//	[ReadOnly(false)]
	//	[CategoryAttribute("Behaviour")]
	//	[Description("Lua script to run when the mouse stays over the object.")]
	//	public string OnHover
	//	{
	//		get { return scriptOnHover; }
	//		set { scriptOnHover = value; }
	//	}

	//	[ReadOnly(false)]
	//	[CategoryAttribute("Behaviour")]
	//	[Description("Lua script to run when the mouse clicks the object.")]
	//	public string OnClick
	//	{
	//		get { return scriptOnClick; }
	//		set { scriptOnClick = value; }
	//	}

	//	[ReadOnly(false)]
	//	[CategoryAttribute("Behaviour")]
	//	[Description("Lua script to run when the mouse moves away from the object.")]
	//	public string OnExit
	//	{
	//		get { return scriptOnExit; }
	//		set { scriptOnExit = value; }
	//	}
	//}
}
