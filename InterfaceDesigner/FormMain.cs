﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Design;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InterfaceDesigner
{
	public partial class FormMain : Form
	{
		///////////////////////////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////////////
		/// Version control.
		///////////////////////////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////////////

		const int VERSION_SIG = 0;	// Significant
		const int VERSION_MAJ = 1;	// Major
		const int VERSION_MIN = 2;	// Minor
		static int[,] version = {
			/// Version 2.2.0
			// - 
			{ 2, 2, 0 },
			/// Version 2.1.2
			// - Fixed bugs in libraries.
			// - Fixed a bug where the list of objects from an interface would persist after a new
			//		interface was opened.
			// - Made the 'FormScriptEditor's interface dynamic (so it resizes and moves elements
			//		when the form is re-sized).
			// - The 'FormScriptEditor' saves the location and size when it closes, and will
			//		re-open at the same location at the same size.
			{ 2, 1, 2 },
			/// Version 2.1.1
			// - Fixed bugs in libraries.
			{ 2, 1, 1 },
			/// Version 2.1.0
			// - Added a 'Preferences' menu option for setting the dimensions of the window that
			//		tests the interface.
			// - Centered 'FormPreferences' and 'FormScriptEditor' when they're shown.
			// - Changed the output folder to 'Win32_$(Configuration)' as '$(Platform)' isn't named
			//		correctly.
			{ 2, 1, 0 },
			/// Version 2.0.0
			// - Implemented saving and loading (with the file extension '.dbif'.
			// - Implemented the menu options ('New', 'Open', 'Save', and 'SaveAs').
			// - Disabled the timer that refresh the DirectX window until AFTER the window has
			//		finished initialising. I believe it was causing the program to crash in release
			//		build.
			// - Implemented movement of objects by clicking and dragging them on the DirectX panel.
			// - Stopped the scene graph from gaining focus when an object was selected on the
			//		DirectX panel. This prevent a bug where the two were fighting for control while
			//		dragging an object around (it means that the object will no longer be
			//		highlighted in the scene graph when it's selected though).
			// - Font colour can now be changed in the property grid.
			// - Fixed a bug where the 'Size' property couldn't be seen or modified for buttons.
			// - Made the 'Location' and 'Size' properties automatically expanded.
			// - Validation is now performed on texture files when they're loaded in to ensure that
			//		they are in 'res/textures/' below the tool's executable.
			// - Implemented a 'Run' macro so that the interface can be tested in the player.
			// - Implemented hotkeys for shortcuts (open, new, save, run, etc.).
			// - Implemented functionality to allow the scaling of objects by click-and-drag in the
			//		DirectX panel.
			// - Fixed a bug where the DirectX panel wouldn't highlight an object when selected via
			//		the scene graph.
			{ 2, 0, 0 },
			/// Version 1.1.0
			// - Objects can now be selected via the DirectX panel.
			// - Objects can be deleted by pressing 'Del' with them selected.
			// - Re-designed the implementation of the 'PropertyGrid' object so that I can
			//		dynamically add/remove properties. The properties are now also unique to object
			//		types (i.e. icons no longer have the 'Font' property).
			// - When a property is changed, only that property should now be sent to C++ (with the
			//		re-design came an easier method of determining which property was changed).
			// - The events are acquired from the C++ library at startup, so the tool won't need
			//		modifying when events are added/removed.
			// - Created a new Form (FormScriptEditor) for editing scripts so that it's easier for
			//		users to type large amounts of text.
			{ 1, 1, 0 },
			/// Version 1.0.0
			// - Design of the interface is complete.
			// - Layout of the interface is dynamic based on the user's resolution (i.e. the values
			//		are fixed on a by-resolution basis).
			// - Objects can be added using the macro buttons.
			// - Property grid displays information pertaining to the selected object and can be
			//		modified.
			{ 1, 0, 0 },
		};


		///////////////////////////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////////////
		/// Static functions.
		///////////////////////////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Comparer for the list of objects so that they can be sorted by value instead of by key.
		/// </summary>
		static int CompareObjectNames(KeyValuePair<int, string> a, KeyValuePair<int, string> b)
		{
			return a.Value.CompareTo(b.Value);
		}


		///////////////////////////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////////////
		/// Member variables of FormMain.
		///////////////////////////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////////////

		//static FormScriptManager scriptManager = new FormScriptManager();

		static InterfaceDesignerWrapper wrapper = new InterfaceDesignerWrapper();

		enum CursorType
		{
			None = 0,
			TopLeft,
			TopRight,
			Top,
			BottomLeft,
			BottomRight,
			Bottom,
			Left,
			Right,
			Middle,
		};
		private CursorType mouseCursor = CursorType.None;

		enum MouseState
		{
			None = 0,
			Icon,
			Text,
			Button,
		};
		private MouseState mouseState = MouseState.None;

		List<KeyValuePair<int, string>> objects = new List<KeyValuePair<int, string>>();

		List<string> objectEvents = new List<string>();
		static int objectEventStringLimit = 0;

		PropertyGridObject objectProps;
		int lastSelectedObject = -1;

		Point mousePosition;

		Preferences prefs = new Preferences();

		/// For saving/loading.
		private const string defaultFileName = "NewInterface.dbif";
		private string formTitle = null;
		private string fileName = null;
		private bool previouslySaved;
		private bool interfaceHasBeenModified;


		///////////////////////////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////////////
		/// Accessors.
		///////////////////////////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////////////

		static public int EventStringLimit
		{
			get { return objectEventStringLimit; }
		}


		///////////////////////////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////////////
		/// Event handlers for FormMain.
		///////////////////////////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Constructor.
		/// </summary>
		public FormMain()
		{
			InitializeComponent();

			// Set the panel splitters to the correct positions. It has to be based on the
			// container and NOT the program dimensions because the 'SplitterDistance' is relative
			// to the container.
			containerAll.SplitterDistance = 0;
			containerLeftRight.SplitterDistance = (containerAll.Width / 5) * 4;
			containerObjects.SplitterDistance = containerAll.Height / 3;

			// Lock the splitters. I've unlocked them in the design view so I can position things,
			// but when the application is running I don't want them to be manipulatable.
			containerAll.IsSplitterFixed = true;
			containerLeftRight.IsSplitterFixed = true;
			// I'm not locking this one so the user can determine what size is best (and it doesn't
			// affect the DirectX panel).
			//containerObjects.IsSplitterFixed = true;

			// Set the border design. I've got this in here because it's easier to change this one
			// variable than manually going through changing them all in the design view.
			BorderStyle design = BorderStyle.Fixed3D;
			containerAll.BorderStyle = design;
			containerLeftRight.BorderStyle = design;
			containerObjects.BorderStyle = design;

			// Set up the open and save file dialog box objects.
			saveFileDialog.InitialDirectory = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
			openFileDialog.InitialDirectory = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
			//openFontDialog.InitialDirectory = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);

			// Disable debug stuff.
			toolStripButton1.Visible = false;
			toolStripLabel1.Visible = false;
		}

		/// <summary>
		/// This is called when the form first opens and contains all the initialisation. I've had
		/// to put the initialisation in here instead of in the constructor because otherwise a
		/// couple of bugs occur. The 'bugs' are because the program is uncertain about
		/// user-specific values, such as screen size.
		/// </summary>
		private void FormMain_Shown(object sender, EventArgs e)
		{
			// Add the version number to the window's title.
			formTitle = this.Text + " " + this.GetVersionString();
			this.SetFormTitle(defaultFileName, false);

			// Size the scene graph and object properties forms.
			containerObjects_SplitterMoved(null, null);

			// Initialise the DirectX application that'll render to the scene panel.
			this.DirectXReload();

			// Load in the event names.
			int index = 0;
			int eventCount = 1;
			while (index < eventCount)
			{
				string eventName = "";
				this.DirectXFuncSucceeded(wrapper._GetObjectEventNames(index, out eventName, out eventCount));

				if (eventName != "")
					objectEvents.Add(eventName);

				++index;
			}
			this.DirectXFuncSucceeded(wrapper._GetObjectEventStringLimit(out objectEventStringLimit));

			timerDirectXRefresh.Enabled = true;
		}

		/// <summary>
		/// The destructor/shutdown function for the application.
		/// </summary>
		private void FormMain_FormClosing(object sender, FormClosingEventArgs e)
		{
			if (interfaceHasBeenModified)
			{
				DialogResult res = MessageBox.Show("You have unsaved changes to \'" + System.IO.Path.GetFileNameWithoutExtension(fileName) + "\'. Do you wish to save these changes before exiting?", "Exiting With Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);

				if (res == DialogResult.Yes)
				{
					if (previouslySaved)
						this.menuBarItemFileSave_Click(menuBarItemFileSave, null);
					else
						this.menuBarItemFileSaveAs_Click(menuBarItemFileSaveAs, null);
				}
				else if (res == DialogResult.Cancel)
				{
					e.Cancel = true;
					return;
				}
			}

			this.DirectXFuncSucceeded( wrapper._Shutdown());
		}

		/// <summary>
		/// Event handler for when the user presses a key.
		/// </summary>
		private void FormMain_KeyDown(object sender, KeyEventArgs e)
		{
			EventArgs args = new EventArgs();

			// Delete the selected object.
			if (e.KeyCode == Keys.Delete)
			{
				if (lastSelectedObject != -1)
				{
					DialogResult res = MessageBox.Show("Are you sure you wish to delete this object?", "Delete Object", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

					if (res == DialogResult.Yes)
						this.DeleteObject(lastSelectedObject);

					e.Handled = true;
				}
			}
			// New.
			else if (e.Control && e.KeyCode == Keys.N)
			{
				this.menuBarItemFileNew_Click( menuBarItemFileNew, args);
				e.Handled = true;
			}
			// Open
			else if (e.Control && e.KeyCode == Keys.O)
			{
				this.menuBarItemFileOpen_Click( menuBarItemFileOpen, args);
				e.Handled = true;
			}
			// Save As
			else if (e.Control && e.Shift && e.KeyCode == Keys.S)
			{
				this.menuBarItemFileSaveAs_Click(menuBarItemFileSaveAs, args);
				e.Handled = true;
			}
			// Save
			else if (e.Control && e.KeyCode == Keys.S)
			{
				this.menuBarItemFileSave_Click(menuBarItemFileSave, args);
				e.Handled = true;
			}
			// Run
			else if (e.KeyCode == Keys.F5)
			{
				this.macroButton_Click(macroRun, args);
				e.Handled = true;
			}
			else if (e.KeyCode == Keys.F)
			{
				this.Cursor = Cursors.Cross;
				e.Handled = true;
			}

		}

		/// <summary>
		/// Event handler for when a macro button is clicked.
		/// This sets what the mouse cursor will do if it's then left-clicked on the DirectX panel.
		/// </summary>
		private void macroButton_Click(object sender, EventArgs e)
		{
			ToolStripButton button = (ToolStripButton)sender;
			this.ClearMouseState();

			if (button == macroAddButton)
			{
				mouseState = MouseState.Button;
				button.Checked = true;
			}
			else if (button == macroAddIcon)
			{
				mouseState = MouseState.Icon;
				button.Checked = true;
			}
			else if (button == macroAddText)
			{
				mouseState = MouseState.Text;
				button.Checked = true;
			}
			else if (button == macroRun)
			{
				// Save the interface to a temporary file.
				string tempFile = System.IO.Path.GetFileName(fileName) + ".temp";
				this.DirectXFuncSucceeded(wrapper._SaveInterface(tempFile));

				// Ensure the player exists.
				string playerPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase) + "\\InterfaceDesignerPlayer.exe";
				playerPath = playerPath.Substring(6);
				if (!System.IO.File.Exists(playerPath))
				{
					MessageBox.Show("Interface Designer player's executable couldn't be found.", "Player Not Found", MessageBoxButtons.OK, MessageBoxIcon.Error);
					return;
				}

				// Start up the 'InterfaceDesignerPlayer' and pass it the path to the interface file.
				System.Diagnostics.Process process = new System.Diagnostics.Process();
				System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
				startInfo.FileName = playerPath;
				startInfo.Arguments = "\"" + tempFile + "\" \"" + prefs.ResX + "\" \"" + prefs.ResY + "\" \"" + prefs.FullScreen + "\"";
				startInfo.UseShellExecute = false;
				//startInfo.RedirectStandardOutput = true;
				//StreamReader reader = process.StandardOutput;
				process.StartInfo = startInfo;
				process.Start();
			}
			else if (button == toolStripButton1)
			{
				if (openFontDialog.ShowDialog() == DialogResult.OK)
				{
					bool res = wrapper._InstallFont(openFontDialog.FileName);

					if (res)
						MessageBox.Show("Font installed successfully!", "Font Installed", MessageBoxButtons.OK, MessageBoxIcon.Information);
					else
						MessageBox.Show("Font could not be installed.", "Font Installation Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
				}
			}
		}

		/// <summary>
		/// Event handler for when a mouse button is pressed over the DirectX panel.
		/// </summary>
		private void panelRender_MouseDown(object sender, MouseEventArgs e)
		{
			Panel panel = (Panel)sender;
			panel.Focus();

			if (e.Button == MouseButtons.Left)
			{
				float x = (float)e.Location.X;
				float y = (float)e.Location.Y;
				this.ConvertMousePositionForDirectX(ref x, ref y);

				// If the user currently has a object macro selected, then place that object in the
				// scene at the cursor's position.
				if (mouseState != MouseState.None)
				{
					this.AddObject(mouseState, x, y);
				}
				// If the user hasn't tried to place an object into the scene, check to see if they
				// have tried to click on an object to select it.
				else
				{
					int id = 0;
					int type = 0;
					this.DirectXFuncSucceeded(wrapper._TestCursorIsOverObject(x, y, out id));
					this.DirectXFuncSucceeded(wrapper._TestCursorPosOverObject(x, y, out type));
					mouseCursor = (CursorType)type;

					int index = 0;
					TreeNodeMouseClickEventArgs click;

					if (id > 0)
					{
						// Find the name associated with the ID and select that on the listView.
						index = this.ListGetIndex(id);
						click = new TreeNodeMouseClickEventArgs(treeSceneGraph.Nodes[index], System.Windows.Forms.MouseButtons.Left, 1, 0, 0);
					}
					else
					{
						click = new TreeNodeMouseClickEventArgs(null, MouseButtons.None, 0, 0, 0);
					}

					treeSceneGraph_NodeMouseClick(treeSceneGraph, click);
				}
			}
			else if (e.Button == MouseButtons.Right)
			{

			}

			this.ClearMouseState();
		}

		/// <summary>
		/// Event handler for when a mouse button is released over the DirectX panel.
		/// </summary>
		private void panelRender_MouseUp(object sender, MouseEventArgs e)
		{
			
		}

		/// <summary>
		/// Event handler for when the user moves the mouse over the DirectX panel.
		/// </summary>
		private void panelRender_MouseEnter(object sender, EventArgs e)
		{

		}

		/// <summary>
		/// Event handler for when the user moves the mouse over the DirectX panel.
		/// </summary>
		private void panelRender_MouseMove(object sender, MouseEventArgs e)
		{
			Panel panel = (Panel)sender;

			// Do nothing if the panel isn't in focus or the left mouse button isn't down.
			if (panel.Focused)
			{
				if (lastSelectedObject != -1)
				{
					float x = (float)e.Location.X;
					float y = (float)e.Location.Y;
					int type = 0;
					this.ConvertMousePositionForDirectX(ref x, ref y);
					this.DirectXFuncSucceeded(wrapper._TestCursorPosOverObject(x, y, out type));

					if (mouseCursor == CursorType.Top || mouseCursor == CursorType.Bottom)
						panel.Cursor = Cursors.SizeNS;
					else if (mouseCursor == CursorType.Left || mouseCursor == CursorType.Right)
						panel.Cursor = Cursors.SizeWE;
					else if (mouseCursor == CursorType.TopLeft || mouseCursor == CursorType.BottomRight)
						panel.Cursor = Cursors.SizeNWSE;
					else if (mouseCursor == CursorType.TopRight || mouseCursor == CursorType.BottomLeft)
						panel.Cursor = Cursors.SizeNESW;
					else if (mouseCursor == CursorType.Middle)
						panel.Cursor = Cursors.SizeAll;
					else if (mouseCursor == CursorType.None)
						panel.Cursor = Cursors.Default;

					if (e.Button == MouseButtons.Left)
					{
						Point move = new Point(e.X - mousePosition.X, e.Y - mousePosition.Y);

						if (mouseCursor == CursorType.Middle)
							this.MoveObjectBy(move.X, move.Y);
						else
							this.ScaleObjectBy(move.X, move.Y, (int)mouseCursor);

						this.DirectXRefresh();
						this.SetFormTitle(fileName, true);
					}
					else
					{
						mouseCursor = (CursorType)type;
					}
				}
			}

			mousePosition = new Point(e.X, e.Y);
		}

		/// <summary>
		/// Event handler for when the cursor is hovering over the render panel.
		/// </summary>
		private void panelRender_MouseHover(object sender, EventArgs e)
		{
			
		}

		/// <summary>
		/// Event handler for when the user clicks a node on the scene graph.
		/// </summary>
		private void treeSceneGraph_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
		{
			// Exit if the user didn't actually select a node.
			TreeNode chosen = e.Node;
			if (chosen == null)
			{
				lastSelectedObject = -1;
			}
			else
			{
				TreeView view = (TreeView)sender;

				view.SelectedNode = e.Node;
				lastSelectedObject = objects[chosen.Index].Key;
			}

			// Indicate to DirectX that this object has been selected.
			this.DirectXFuncSucceeded(wrapper._SetSelectedObject(lastSelectedObject));

			// Get all the object's information and display it in the properties form.
			this.UpdateObjectPropertiesForm(lastSelectedObject);
		}

		/// <summary>
		/// Event handler for when the splitter between the scene graph and object properties
		/// windows is moved. This re-sizes the forms to suit their new areas.
		/// </summary>
		private void containerObjects_SplitterMoved(object sender, SplitterEventArgs e)
		{
			// Scene graph.
			lblTitleSceneGraph.Location = new Point(3, 3);
			lblTitleSceneGraph.Width = panelSceneGraph.Width - 6;
			treeSceneGraph.Location = new Point(0, lblTitleSceneGraph.Height + 6);
			treeSceneGraph.Width = panelSceneGraph.Width;
			treeSceneGraph.Height = panelSceneGraph.Height - (lblTitleSceneGraph.Height + 6);

			// Object properties.
			lblTitleObjectProps.Location = new Point(3, 3);
			lblTitleObjectProps.Width = panelObjectProps.Width - 6;
			propsObjectProps.Location = new Point(0, lblTitleObjectProps.Height + 6);
			propsObjectProps.Width = panelObjectProps.Width;
			propsObjectProps.Height = panelObjectProps.Height - (lblTitleObjectProps.Height + 6);
		}

		/// <summary>
		/// Event handler for when an object's property is modified.
		/// </summary>
		private void propsObjectProps_PropertyValueChanged(object s, PropertyValueChangedEventArgs e)
		{
			int id = lastSelectedObject;

			if (e.ChangedItem.Label == "Name")
			{
				// If the object's name has changed, then apply that change as well as updating its
				// name in the scene graph.
				this.DirectXFuncSucceeded(wrapper._SetObjectNameFromID(id, (string)e.ChangedItem.Value));

				this.ListDeleteByKey(id);
				this.ListAddByKey(new KeyValuePair<int, string>(id, (string)e.ChangedItem.Value));
			}
			else if (e.ChangedItem.Label == "Location" || e.ChangedItem.Parent.Label == "Location")
			{
				Vector3 pos = (Vector3)objectProps.GetProperty("Location").m_value;
				this.DirectXFuncSucceeded(wrapper._SetObjectLocationFromID(id, pos.X, pos.Y, pos.Z));
			}
			else if (e.ChangedItem.Label == "Size" || e.ChangedItem.Parent.Label == "Size")
			{
				SizeF size = (SizeF)objectProps.GetProperty("Size").m_value;
				this.DirectXFuncSucceeded(wrapper._SetObjectSizeFromID(id, size.Width, size.Height));
			}
			else if (e.ChangedItem.Label == "Texture")
			{
				string texture = (string)e.ChangedItem.Value;
				if (this.ValidateTexturePath(texture))
					this.DirectXFuncSucceeded(wrapper._SetObjectTexturePathFromID(id, texture));
				else
					objectProps.GetProperty(e.ChangedItem.Label).m_value = (string)e.OldValue;
			}
			else if (e.ChangedItem.Label == "Text")
			{
				string text = (string)e.ChangedItem.Value;
				this.DirectXFuncSucceeded(wrapper._SetObjectTextFromID(id, text));
			}
			else if (e.ChangedItem.Label == "Font")
			{
				Font font = (Font)e.ChangedItem.Value;
			}
			else if (e.ChangedItem.Label == "Font Colour")
			{
				Color colour = (Color)e.ChangedItem.Value;
				this.DirectXFuncSucceeded(wrapper._SetObjectFontColourFromID(id, colour.R, colour.G, colour.B, colour.A));
			}
			else if (e.ChangedItem.Parent.Label == "Behaviour")
			{
				int i = 0;
				for (; i < objectEvents.Count; ++i)
					if (objectEvents[i] == (string)e.ChangedItem.Label)
						break;

				if (i != objectEvents.Count)
					this.DirectXFuncSucceeded(wrapper._SetObjectEventScriptFromID(id, objectEvents[i], (string)e.ChangedItem.Value));
			}

			// Get the new properties.
			objectProps = (PropertyGridObject)propsObjectProps.SelectedObject;

			this.SetFormTitle(fileName, true);
		}

		/// <summary>
		/// This timer keeps the DirectX panel being updated and rendered regularly.
		/// </summary>
		private void timerDirectXRefresh_Tick(object sender, EventArgs e)
		{
			this.DirectXRefresh();
		}


		///////////////////////////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////////////
		/// Menu Bar Event Handlers.
		///////////////////////////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Event handler for when the user clicks 'File > New'.
		/// </summary>
		private void menuBarItemFileNew_Click(object sender, EventArgs e)
		{
			if (interfaceHasBeenModified)
			{
				DialogResult res = MessageBox.Show("This interface is about to be cleared. Would you like to continue?", "New Interface", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

				if (res == DialogResult.No)
					return;
			}

			// Delete the objects from the interface.
			this.ClearInterface();

			previouslySaved = false;
			this.SetFormTitle(defaultFileName, false);
		}

		/// <summary>
		/// Event handler for when the user clicks 'File > Open'.
		/// </summary>
		private void menuBarItemFileOpen_Click(object sender, EventArgs e)
		{
			DialogResult res;
			if (interfaceHasBeenModified)
			{
				res = MessageBox.Show("The scene is about to be overwritten. Would you like to continue?", "Modified Scene", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

				if (res == DialogResult.No)
					return;
			}

			res = openFileDialog.ShowDialog();

			if (res != DialogResult.OK)
				return;

			this.DirectXFuncSucceeded(wrapper._LoadInterface(openFileDialog.FileName));

			// Load the objects into C#.
			int count = 0;
			this.DirectXFuncSucceeded(wrapper._GetObjectCount(out count));
			this.ListClear();
			for (int i = 0; i < count; ++i)
			{
				int id = 0;
				string name = null;

				this.DirectXFuncSucceeded(wrapper._GetObjectAtIndex(i, out id, out name));
				this.ListAddByKey(new KeyValuePair<int, string>(id, name));
			}

			lastSelectedObject = -1;
			this.UpdateSceneGraph();
			this.UpdateObjectPropertiesForm(lastSelectedObject);

			previouslySaved = true;
			this.SetFormTitle(openFileDialog.FileName, false);
		}

		/// <summary>
		/// Event handler for when the user clicks 'File > Save'.
		/// </summary>
		private void menuBarItemFileSave_Click(object sender, EventArgs e)
		{
			if (!previouslySaved)
			{
				this.menuBarItemFileSaveAs_Click(menuBarItemFileSaveAs, e);
				return;
			}

			this.DirectXFuncSucceeded(wrapper._SaveInterface(fileName));

			MessageBox.Show("Interface saved successfully!", "Save Successful", MessageBoxButtons.OK);

			this.SetFormTitle(fileName, false);
		}

		/// <summary>
		/// Event handler for when the user clicks 'File > Save As'.
		/// </summary>
		private void menuBarItemFileSaveAs_Click(object sender, EventArgs e)
		{
			saveFileDialog.FileName = System.IO.Path.GetFileNameWithoutExtension(fileName);
			DialogResult res = saveFileDialog.ShowDialog();

			if (res != DialogResult.OK)
				return;

			previouslySaved = true;
			this.DirectXFuncSucceeded(wrapper._SaveInterface(saveFileDialog.FileName));

			MessageBox.Show("Interface saved successfully!", "Save Successful", MessageBoxButtons.OK);

			this.SetFormTitle(saveFileDialog.FileName, false);
		}

		/// <summary>
		/// Event handler for when the user click 'Script Editor' on the menu.
		/// </summary>
		private void menuBarItemScriptManager_Click(object sender, EventArgs e)
		{
			FormScriptManager scriptManager = new FormScriptManager();
			scriptManager.Show();
		}

		/// <summary>
		/// Event handler for when the user clicks 'Preferences' on the menu.
		/// </summary>
		private void menuBarItemPreferences_Click(object sender, EventArgs e)
		{
			FormPreferences f = new FormPreferences( prefs);
			f.ShowDialog();
		}

		/// <summary>
		/// Event handler for when the user clicks 'About' on the menu.
		/// </summary>
		private void menuBarItemAbout_Click(object sender, EventArgs e)
		{
			string about = "The Deathbetray Engine Interface Designer.\n\n" +
							"Created by: Edward Willoughby\n" +
							"Version: " + this.GetVersionString() + "\n";

			MessageBox.Show(about, "About", MessageBoxButtons.OK, MessageBoxIcon.Information);
		}


		///////////////////////////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////////////
		/// Private utility functions.
		///////////////////////////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Gets the current version number of the tool in string format.
		/// </summary>
		private string GetVersionString()
		{
			return "v" + version[0, VERSION_SIG] + "." + version[0, VERSION_MAJ] + "." + version[0, VERSION_MIN];
		}

		/// <summary>
		/// Sets the title for the form based on the current scene's name (i.e. the file that's
		/// open) and includes an asterisk if the scene has been modified since the last save. If
		/// you don't want to change either the file name or the modified boolean when calling this
		/// function, just use the global variable as the parameter.
		/// </summary>
		private void SetFormTitle(string file, bool modified)
		{
			fileName = file;
			interfaceHasBeenModified = modified;

			this.Text = System.IO.Path.GetFileNameWithoutExtension(fileName) + (interfaceHasBeenModified ? "*" : "") + " - " + formTitle;
		}

		/// <summary>
		/// Converts the mouse's position (when it's over the DirectX panel) to screen coordinates
		/// that match what the DirectX library uses.
		/// </summary>
		private void ConvertMousePositionForDirectX(ref float x, ref float y)
		{
			x = x / (float)panelRender.Width;
			y = y / (float)panelRender.Height;

			x = x - 0.5f;
			y = (y * -1.0f) + 0.5f;
		}

		/// <summary>
		/// Adds an object to the interface at the specified position.
		/// </summary>
		private void AddObject(MouseState type, float x, float y)
		{
			int id = -1;
			string text = null;

			if (type == MouseState.Button)
			{
				text = "newButton";
				this.DirectXFuncSucceeded(wrapper._AddObjectButton(out id, text, x, y));
			}
			else if (type == MouseState.Icon)
			{
				text = "newIcon";
				this.DirectXFuncSucceeded(wrapper._AddObjectIcon(out id, text, x, y));
			}
			else if (type == MouseState.Text)
			{
				text = "newText";
				this.DirectXFuncSucceeded(wrapper._AddObjectText(out id, text, x, y));
			}

			if (id != -1)
			{
				this.ListAddByKey(new KeyValuePair<int, string>(id, text));
				this.SetFormTitle(fileName, true);
			}
		}

		/// <summary>
		/// Deletes an object from the scene.
		/// </summary>
		private void DeleteObject(int id)
		{
			lastSelectedObject = -1;

			this.ListDeleteByKey(id);

			this.DirectXFuncSucceeded(wrapper._DeleteObject(id));
			this.UpdateSceneGraph();
			this.UpdateObjectPropertiesForm(-1);

			this.SetFormTitle(fileName, true);
		}

		/// <summary>
		/// Clears the interface of all contents and refreshes the forms.
		/// </summary>
		private void ClearInterface()
		{
			this.DirectXFuncSucceeded(wrapper._DeleteAllObjects());
			objects.Clear();
			treeSceneGraph.Nodes.Clear();
			propsObjectProps.SelectedObject = null;
		}

		/// <summary>
		/// Moves an object by the specified amount.
		/// </summary>
		private void MoveObjectBy(int px, int py)
		{
			int id = lastSelectedObject;
			float x = px / (float)panelRender.Width;
			float y = (py / (float)panelRender.Height) * -1.0f;

			this.DirectXFuncSucceeded(wrapper._MoveObjectBy(id, x, y));

			this.UpdateObjectPropertiesForm(id);
		}

		/// <summary>
		/// Scales an object by the specified amount.
		/// </summary>
		private void ScaleObjectBy(int px, int py, int dir)
		{
			int id = lastSelectedObject;
			float x = px / (float)panelRender.Width;
			float y = (py / (float)panelRender.Height) * -1.0f;

			this.DirectXFuncSucceeded(wrapper._ScaleObjectBy(id, x, y, dir));

			this.UpdateObjectPropertiesForm(id);
		}

		/// <summary>
		/// Re-populates the scene graph with all objects in the scene.
		/// </summary>
		private void UpdateSceneGraph()
		{
			treeSceneGraph.Nodes.Clear();

			foreach (KeyValuePair<int, string> pair in objects)
				treeSceneGraph.Nodes.Add(pair.Value);
		}

		/// <summary>
		/// Updates the list of properties for the selected object. The parameter is not the
		/// position of the selected object in the treeView, but instead the unique ID of the
		/// object in the 'objects' list. Therefore, it can be passed directly to the C++ library
		/// and SHOULDN'T be invalid.
		/// </summary>
		private void UpdateObjectPropertiesForm(int id)
		{
			if (id == -1)
			{
				propsObjectProps.SelectedObject = null;
				objectProps = null;
				return;
			}

			string name;
			string type;
			float x, y, z;
			float width, height;
			string texture;
			string text;
			int r, g, b, a;

			this.DirectXFuncSucceeded(wrapper._GetObjectNameFromID(id, out name));
			this.DirectXFuncSucceeded(wrapper._GetObjectTypeFromID(id, out type));
			this.DirectXFuncSucceeded(wrapper._GetObjectLocationFromID(id, out x, out y, out z));
			this.DirectXFuncSucceeded(wrapper._GetObjectSizeFromID(id, out width, out height));
			this.DirectXFuncSucceeded(wrapper._GetObjectTexturePathFromID(id, out texture));
			this.DirectXFuncSucceeded(wrapper._GetObjectTextFromID(id, out text));
			this.DirectXFuncSucceeded(wrapper._GetObjectFontColourFromID(id, out r, out g, out b, out a));

			Vector3 vec = new Vector3();
			vec.X = x;
			vec.Y = y;
			vec.Z = z;

			objectProps = new PropertyGridObject();
			objectProps.Add(new ObjectProperty("Attribute", "ID", "The unique identifier for this object", typeof(int), id, true));
			objectProps.Add(new ObjectProperty("Attribute", "Name", "The object's name.", typeof(string), name, false));
			objectProps.Add(new ObjectProperty("Attribute", "Type", "The type of object.", typeof(string), type, true));

			objectProps.Add(new ObjectProperty("Appearance", "Location", "The object's position on the screen (in screen coordinates).", typeof(Vector3), vec, false));
			if (type != "Text")
			{
				objectProps.Add(new ObjectProperty("Appearance", "Size", "The dimensions of the object.", typeof(SizeF), new SizeF(width, height), false));
				objectProps.Add(new ObjectProperty("Appearance", "Texture", "The path of the texture used for this object.", typeof(FileSelectorTypeEditor), texture, false));
			}
			if (type != "Icon")
			{
				objectProps.Add(new ObjectProperty("Appearance", "Text", "The text displayed by this object.", typeof(string), text));
				//objectProps.Add(new ObjectProperty("Appearance", "Font", "The font used by the text.", typeof(Font), new Font("Arial", 8.0f, FontStyle.Regular), false));
				objectProps.Add(new ObjectProperty("Appearance", "Font Colour", "The colour used by the font.", typeof(ColourPickerTypeEditor), Color.FromArgb( a, r, g, b), false));
			}

			string eventDesc = "Lua script to run when this event occurs.";
			for (int i = 0; i < objectEvents.Count; ++i)
			{
				string script = "";
				this.DirectXFuncSucceeded(wrapper._GetObjectEventScriptFromID(id, objectEvents[i], out script));
				objectProps.Add(new ObjectProperty("Behaviour", objectEvents[i], eventDesc, typeof(EventScript), script, false));
			}

			propsObjectProps.SelectedObject = objectProps;

			// Auto-expand certain properties.
			GridItem root = propsObjectProps.SelectedGridItem;
			while( root.Parent != null)
				root = root.Parent;

			foreach (GridItem category in root.GridItems)
			{
				foreach (GridItem item in category.GridItems)
				{
					if (item.Label == "Location" || item.Label == "Size")
						item.Expanded = true;
				}
			}
		}

		/// <summary>
		/// Ensures a string correctly points to a valid texture.
		/// </summary>
		private bool ValidateTexturePath(string texture)
		{
			if (File.Exists(texture))
				if (System.IO.Path.GetExtension(texture) == ".dds")
					return true;

			return false;
		}

		/// <summary>
		/// Clears the mouse state and all of the macros' states.
		/// </summary>
		private void ClearMouseState()
		{
			mouseState = MouseState.None;

			foreach (ToolStripItem item in macroBar.Items)
			{
				try
				{
					ToolStripButton button = (ToolStripButton)item;
					button.Checked = false;
				}
				catch { /* Nothing... I don't care. */ }
			}
		}


		///////////////////////////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////////////
		/// List Functions.
		///////////////////////////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Adds an item to the list of objects by its key.
		/// </summary>
		private void ListAddByKey(KeyValuePair<int, string> v)
		{
			IEnumerable<KeyValuePair<int, string>> result;
			result = objects.Where(obj => obj.Key == v.Key);
			bool exists = false;

			// This is a really stupid fucking way of determining whether or not any elements exist,
			// but apparently C#'s mother was also it's sister.
			foreach (var pair in result)
				if (pair.Key == v.Key)
					exists = true;

			if (!exists)
			{
				objects.Add(v);
				this.ListSortByValues();
			}
		}

		/// <summary>
		/// Deletes an item from the list of objects.
		/// </summary>
		private void ListDeleteByKey(int k)
		{
			int index = this.ListGetIndex(k);

			if( index != -1)
				objects.Remove(objects[index]);
		}

		/// <summary>
		/// Deletes all the items from the list of objects.
		/// </summary>
		private void ListClear()
		{
			objects.Clear();
		}

		/// <summary>
		/// Gets the index position of a pair from the key.
		/// </summary>
		private int ListGetIndex(int k)
		{
			return objects.FindIndex(x => x.Key == k);
		}

		/// <summary>
		/// Gets a pair from the key.
		/// </summary>
		private KeyValuePair<int, string> ListGetPair(int k)
		{
			return objects.Find(obj => obj.Key == k);
		}

		/// <summary>
		/// Sorts the items in the list of objects by their value.
		/// </summary>
		private void ListSortByValues()
		{
			objects.Sort(CompareObjectNames);

			this.UpdateSceneGraph();
		}


		///////////////////////////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////////////
		/// DirectX Functions.
		///////////////////////////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>
		/// Reloads DirectX. This can be called to initialise DirectX the first time as well
		/// because if DirectX hasn't been initialised yet, then '_Shutdown' simply won't do
		/// anything except return true.
		/// </summary>
		private void DirectXReload()
		{
			//this.DirectXFuncSucceeded(wrapper._Shutdown());
			this.DirectXFuncSucceeded(wrapper._Initialise(panelRender.Handle));
		}

		/// <summary>
		/// This function contains everything that is required to refresh the DirectX panel. I have
		/// to put the '_RefreshFrame()' function inside this one because there'll be other things
		/// that need to be done before that function is called.
		/// </summary>
		private void DirectXRefresh()
		{
			this.DirectXFuncSucceeded(wrapper._UpdateAndRender());
		}

		/// <summary>
		/// Checks that the DirectX function exited correctly and didn't produce a bug... at least
		/// not from the return value.
		/// </summary>
		private void DirectXFuncSucceeded(bool result)
		{
			if (result)
				return;

			// If the function failed then display a message and then hit a breakpoint so debugging
			// can be done.
			MessageBox.Show("A DirectX function failed to run!", "DirectX Failure", MessageBoxButtons.OK);
			System.Diagnostics.Debugger.Break();
		}
	}
}
