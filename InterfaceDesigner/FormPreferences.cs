﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InterfaceDesigner
{
	public partial class FormPreferences : Form
	{
		Preferences prefs;

		int[,] resolutions = {
			{ 800, 600 },
			{ 1024, 768 },
			{ 1280, 720 },
			{ 1280, 1024 },
			{ 1920, 1080 },
		};

		/// <summary>
		/// Constructor.
		/// </summary>
		public FormPreferences( Preferences p)
		{
			InitializeComponent();

			prefs = p;

			for (int i = 0; i < resolutions.Length / 2; ++i)
				comboResolution.Items.Add(resolutions[i, 0] + " x " + resolutions[i, 1]);

			comboResolution.SelectedIndex = comboResolution.FindStringExact(prefs.ResX + " x " + prefs.ResY);
			cbFullScreen.Checked = prefs.FullScreen;
		}

		private void FormPreferences_Shown(object sender, EventArgs e)
		{
			Rectangle rect = Screen.PrimaryScreen.Bounds;
			Point pos = new Point((rect.Width / 2) - (this.Width / 2), (rect.Height / 2) - (this.Height / 2));
			this.Location = pos;
		}

		private void btnConfirm_Click(object sender, EventArgs e)
		{
			int index = comboResolution.SelectedIndex;

			prefs.ResX = resolutions[index, 0];
			prefs.ResY = resolutions[index, 1];
			prefs.FullScreen = cbFullScreen.Checked;

			this.Close();
		}

		private void btnCancel_Click(object sender, EventArgs e)
		{
			this.Close();
		}
	}

	public class Preferences
	{
		int resX;
		int resY;

		bool fullScreen;

		public Preferences()
		{
			resX = 800;
			resY = 600;
			fullScreen = false;
		}

		public int ResX
		{
			get { return resX; }
			set { resX = value; }
		}

		public int ResY
		{
			get { return resY; }
			set { resY = value; }
		}

		public bool FullScreen
		{
			get { return fullScreen; }
			set { fullScreen = value; }
		}
	}
}
