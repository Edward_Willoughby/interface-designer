/********************************************************************
*	Function definitions for the InterfaceContainer class.
********************************************************************/

/********************************************************************
*	Include the header file.
********************************************************************/
#include "InterfaceContainer.h"

#include <fstream>

#include <DBEApp.h>
#include <DBECamera.h>
#include <DBEColour.h>
#include <DBEFont.h>
#include <DBEUITexture.h>

#include "InterfaceObject.h"
/********************************************************************
*	Defines, constants, namespaces, and local variables.
********************************************************************/
using namespace DBE;
using namespace DirectX;

/// Pointer to the InterfaceContainer that is currently being updated.
/// The purpose of this pointer to is give the Lua functions a way to access the container.
static InterfaceContainer* gsp_interfaceBeingUpdated = nullptr;

/// Indicates whether or not the Lua function have already been added to the manager.
static bool gs_luaFunctionsRegistered = false;

/// Stores the strings for the different versions of the saving/loading system.
static const char* gsc_fileVersion[] = {
	"100",
};
/// Constant for how many versions of the saving/loading system there are.
static const s32 gsc_fileVersionCount = sizeof( gsc_fileVersion) / sizeof( gsc_fileVersion[0]);


/**
* Deals with initialisation.
*
* @return True if everything initialised correctly.
*/
bool InterfaceContainer::Init( DBE::App* p_app, InterfaceFuncPointers funcPointers) {
	mp_app = p_app;
	m_fp = funcPointers;

	mp_icon = new UITexture();
	mp_cam = new Camera();
	//mp_cam->m_position = Vec3( 0.0f, 10.0f, -10.0f);
	mp_cam->m_position = Vec3( 0.0f, 18.0f, -12.0f);
	mp_cam->m_lookAt = Vec3( 0.0f, 0.0f, -3.0f);

	m_objBeingUpdated = -1;

	if( !gs_luaFunctionsRegistered) {
		MGR_LUA().RegisterFunction( "DeleteObject", LuaFunction_ContainerDeleteObject);
		MGR_LUA().RegisterFunction( "DeleteAllObjects", LuaFunction_ContainerDeleteAllObjects);
		MGR_LUA().RegisterFunction( "LoadInterface", LuaFunction_ContainerLoadInterface);

		MGR_LUA().RegisterFunction( "SetTexture", LuaFunction_ContainerSetTexture);
		MGR_LUA().RegisterFunction( "SetText", LuaFunction_ContainerSetText);
		MGR_LUA().RegisterFunction( "SetFontColour", LuaFunction_ContainerSetFontColour);
		MGR_LUA().RegisterFunction( "SetEventScript", LuaFunction_ContainerSetEventScript);

		MGR_LUA().RegisterFunction( "GetObjectID", LuaFunction_ContainerGetObjectID);
		MGR_LUA().RegisterFunction( "GetThisObjectID", LuaFunction_ContainerGetThisObjectID);
		MGR_LUA().RegisterFunction( "GetName", LuaFunction_ContainerGetName);
		MGR_LUA().RegisterFunction( "GetTexture", LuaFunction_ContainerGetTexture);
		MGR_LUA().RegisterFunction( "GetText", LuaFunction_ContainerGetText);
		//MGR_LUA().RegisterFunction( "GetFontColour", LuaFunction_ContainerGetFontColour);
		MGR_LUA().RegisterFunction( "GetEventScript", LuaFunction_ContainerGetEventScript);
	}

	return true;
}

/**
* Deals with any updating.
*
* @param deltaTime :: The time taken to render the last frame.
*
* @return True if everything updated correctly.
*/
bool InterfaceContainer::Update( float deltaTime) {
	gsp_interfaceBeingUpdated = this;
	mp_cam->Update();

	this->SortObjectsByZDistance();

	m_objBeingUpdated = 0;
	for( ContainerObjectList::iterator it( mp_objects.begin()); it != mp_objects.end(); ++it) {
		++m_objBeingUpdated;
		(*it)->Update( deltaTime);

		if( m_fp.objectUpdate != nullptr)
			m_fp.objectUpdate( &(*it));
	}
	m_objBeingUpdated = -1;
	gsp_interfaceBeingUpdated = nullptr;

	return true;
}

/**
* Deals with any rendering.
*
* @param deltaTime :: The time taken to render the last frame.
*
* @return True if everything rendered correctly.
*/
bool InterfaceContainer::Render( float deltaTime) {
	mp_cam->ApplyOrthoMatrixLH( 1.0f, 1000.0f);

	GET_APP()->SetBlendState( true);
	GET_APP()->SetDepthStencilState( true, false);
	GET_APP()->SetRasteriserState( false, false);

	for( ContainerObjectList::iterator it( mp_objects.begin()); it != mp_objects.end(); ++it) {
		InterfaceObject* obj( (*it));
		if( obj == nullptr)
			continue;

		switch( obj->GetType()) {
			case InterfaceObjectType::IO_Text:
				DEFAULT_FONT()->DrawString2D( obj->GetPosition(), obj->GetRotation(), 1.0f, &obj->GetFontStyle(), obj->GetText());
				break;

			case InterfaceObjectType::IO_Button:
			case InterfaceObjectType::IO_Icon:
				mp_icon->MoveTo( obj->GetPosition());
				mp_icon->RotateTo( obj->GetRotation());
				mp_icon->ScaleTo( obj->GetScale());
				mp_icon->mp_texture = obj->GetTexture();

				mp_icon->Update( deltaTime);
				mp_icon->Render( deltaTime);

				if( obj->GetType() == IO_Button) {
					Vec2 sMin, sMax;
					DEFAULT_FONT()->CalculateTextDimensions( obj->GetText(), Vec3( 0.0f), sMin, sMax);
					float halfWidth = (sMax.GetX() - sMin.GetX()) / 2.0f;
					float halfHeight = (sMax.GetY() - sMin.GetY()) / 2.0f;

					DEFAULT_FONT()->DrawString2D( obj->GetPosition() - Vec3( halfWidth, halfHeight, 0.0f), obj->GetRotation(), 1.0f, &obj->GetFontStyle(), obj->GetText());
				}
				break;
		}
	}

	return true;
}

/**
* Deals with anything that needs to be released or shutdown.
*/
void InterfaceContainer::Shutdown() {
	// The texture should be deleted within the 'InterfaceObject' object.
	mp_icon->mp_texture = nullptr;
	SafeDelete( mp_icon);

	SafeDelete( mp_cam);

	this->DeleteAllObjects();
}

/**
* Saves the current interface to the specified file.
*
* @param file :: The path to save the interface to.
*
* @return True if no errors occurred.
*/
bool InterfaceContainer::SaveInterface( const char* file) const {
	return this->SaveUsing_100( file);
}


/**
* Loads an interface from the specified file.
* NOTE: This function assumes that the 'InterfaceContainer' has already been cleared of objects.
*
* @param file :: The file to load the interface from.
*
* @return True if no errors occurred.
*/
bool InterfaceContainer::LoadInterface( const char* file) {
	std::ifstream inf( file, std::fstream::in);
	bool res( false);

	// Get the version number of the file.
	char buffer[256];
	inf.getline( buffer, 256);
	inf.close();

	// Find which version this file uses.
	if( strcmp( buffer, gsc_fileVersion[0]) == 0)
		res = this->LoadUsing_100( file);

	if( res)
		if( m_fp.objectDeleted != nullptr)
			m_fp.objectDeleted();

	return res;
}

/**
* Gets the number of objects currently held by the container.
*
* @return The number of objects currently held by the container.
*/
s32 InterfaceContainer::GetNumberOfObjects() const {
	return mp_objects.size();
}

/**
* Get the object at the specified position in the array.
* NOTE: The return value may be a nullptr as this function directly returns the values from the
* the array positions.
*
* @param index :: The position in the array.
*
* @return Pointer to the 'InterfaceObject' at the position in the array.
*/
InterfaceObject* InterfaceContainer::GetObjectAt( s32 index) const {
	InterfaceObject* obj( nullptr);

	if( index < s32( mp_objects.size())) {
		ContainerObjectList::const_iterator it( mp_objects.begin());
		for( index; index > 0; --index)
			++it;

		obj = (*it);
	}

	return obj;
}

/**
* Adds a new object to the container.
*
* @param p_obj :: The new object to be added.
*
* @return True if the object was added correctly.
*/
bool InterfaceContainer::AddNewObject( InterfaceObject* p_obj) {
	DBE_Assert( p_obj != nullptr);

	// Early out if there are already too many objects.
	if( mp_objects.size() >= OBJ_COUNT)
		return false;

	// Exit if the ID is a duplicate.
	for( ContainerObjectList::iterator it( mp_objects.begin()); it != mp_objects.end(); ++it)
		if( (*it)->GetUniqueID() == p_obj->GetUniqueID())
			return false;

	mp_objects.push_back( p_obj);

	return true;
}

/**
* Deletes an existing object in the container.
*
* @param name :: The name of the object.
*
* @return True if the object was deleted.
*/
bool InterfaceContainer::DeleteObject( s32 id) {
	bool deleted( false);
	for( ContainerObjectList::iterator it( mp_objects.begin()); it != mp_objects.end(); ++it) {
		if( (*it)->GetUniqueID() == id) {
			SafeDelete( (*it));
			mp_objects.erase( it);
			deleted = true;
			break;
		}
	}

	if( deleted)
		if( m_fp.objectDeleted != nullptr)
			m_fp.objectDeleted();

	return true;
}

/**
* Deletes all of the existing objects in the container.
*/
void InterfaceContainer::DeleteAllObjects() {
	if( mp_objects.size() == 0)
		return;

	for( ContainerObjectList::iterator it( mp_objects.begin()); it != mp_objects.end(); ++it)
		SafeDelete( *it);

	mp_objects.clear();

	if( m_fp.objectDeleted != nullptr)
		m_fp.objectDeleted();
}

/**
* Gets an ID that doesn't already exist in the array of objects. This must be used to initialise an
* 'InterfaceObject' when created and cannot be changed after initialisation. Also note that if an
* 'InterfaceObject' is created with this ID and not added to the container before querying this
* function again, then it'll likely return the same value. This will result, when the two objects
* are added to the container, in the second object not being added due to a duplicate ID being used.
* Also note that the ID MUST NOT be 0, due to an issue in the C# tool.
*
* @return An 'InterfaceObject' ID that is not currently in use. Returns -1 if no valid ID can be
*			found, or the container already has the maximum number of objects.
*/
s32 InterfaceContainer::GetAvailableUniqueID() const {
	s32 id( 1);
	bool valid( false);

	// Early out if the container is already at its limit. If not, then it should have space for
	// another object.
	if( this->GetNumberOfObjects() == OBJ_COUNT)
		return -1;

	do {
		// Check if the id already exists in the array.
		bool exists( false);
		for( ContainerObjectList::const_iterator it( mp_objects.begin()); it != mp_objects.end(); ++it) {
			if( (*it)->GetUniqueID() == id) {
				exists = true;
			}
		}

		// If the for loop broke-out, then increment the id and try again.
		if( exists)
			++id;
		// If it didn't, then the id isn't a duplicate.
		else
			valid = true;
	} while( !valid);

	return id;
}

/**
* Searches for an existing object in the container using its unique ID.
*
* @param id :: The ID of the object.
*
* @return Pointer to the object. Returns 'nullptr' if the object isn't found.
*/
InterfaceObject* InterfaceContainer::GetObjectByID( s32 id) {
	if( id == -1 || id == 0)
		return nullptr;

	InterfaceObject* obj( nullptr);

	for( ContainerObjectList::iterator it( mp_objects.begin()); it != mp_objects.end(); ++it) {
		if( (*it)->GetUniqueID() == id) {
			obj = (*it);
			break;
		}
	}

	return obj;
}

/**
* Searches for an existing object in the container using its name.
*
* @param name :: The name of the object.
*
* @return Pointer to the object. Returns 'nullptr' if the object isn't found.
*/
InterfaceObject* InterfaceContainer::GetObjectByName( const char* name) {
	if( name == nullptr)
		return nullptr;

	InterfaceObject* obj( nullptr);

	for( ContainerObjectList::iterator it( mp_objects.begin()); it != mp_objects.end(); ++it) {
		if( strcmp( (*it)->GetName(), name) == 0) {
			obj = (*it);
			break;
		}
	}

	return obj;
}

/**
* Gets the object currently being updated. This should only be used inside Lua functions so
* modifications can be done directly to the object (i.e. setting the object's texture, changing its
* text, etc.).
*
* @return Pointer to the object (nullptr if none is currently being updated).
*/
InterfaceObject* InterfaceContainer::GetObjectBeingUpdated() {
	if( m_objBeingUpdated == -1)
		return nullptr;

	return this->GetObjectAt( m_objBeingUpdated);
}

/**
* Gets the ID of the object currently being updated. This should only be used inside Lua functions
* so modifications can be done directly to the object (i.e. setting the object's texture, changing
* its text, etc.).
*
* @return ID of the object (-1 if none is currently being updated).
*/
s32 InterfaceContainer::GetObjectBeingUpdatedID() {
	return m_objBeingUpdated;
}

/**
* 
*/
ContainerObjectList::const_iterator InterfaceContainer::GetConstIterator() const {
	return mp_objects.cbegin();
}

/**
* 
*/
ContainerObjectList::const_reverse_iterator InterfaceContainer::GetConstRIterator() const {
	return mp_objects.crbegin();
}

/**
* 
*/
void InterfaceContainer::SortObjectsByZDistance() {
	mp_objects.sort( InterfaceObject::Closer);
}

/**
* Enables/Disables the objects executing their Lua event scripts.
*
* @param useScripts :: True to enable the execution of scripts.
*/
void InterfaceContainer::UseLuaScripts( bool useScripts) {
	InterfaceObject::UseLuaScripts( useScripts);
}

/**
* Enables/Disables the objects checking their events.
*
* @param enableEvents :: True to allow the objects to check their events.
*/
void InterfaceContainer::EnableObjectEvents( bool enableEvents) {
	InterfaceObject::EnableEvents( enableEvents);
}

/**
* Save version 100.
*
* @param file :: The path to save the interface to.
*
* @return True if no errors occurred.
*/
bool InterfaceContainer::SaveUsing_100( const char* file) const {
	std::ofstream out( file);
	s32 scriptLength( gsc_luaScriptLength);
	
	// Early out if the file doesn't exist/couldn't be opened.
	if( !out.is_open())
		return false;

	// Output the version.
	out << gsc_fileVersion[0] << "\n";

	// Output the number of objects.
	out << mp_objects.size() << "\n";

	for( ContainerObjectList::const_iterator it( mp_objects.begin()); it != mp_objects.end(); ++it) {
		InterfaceObject* obj( *it);
		Vec3 pos( obj->GetPosition());
		Vec3 scale( obj->GetScale());

		out << obj->GetUniqueID() << "\n"
			<< obj->GetType() << "\n"
			<< obj->GetName() << "\n"
			<< obj->GetTexture()->m_fileName << "\n"
			<< strlen( obj->GetText()) << " " << obj->GetText() << "\n"
			<< obj->GetFontColour() << "\n"
			<< pos.GetX() << " " << pos.GetY() << " " << pos.GetZ() << "\n"
			<< scale.GetX() << " " << scale.GetY() << " " << scale.GetZ() << "\n";

		// Output the event scripts.
		out << ObjectEvent::OE_Count << "\n";
		for( s32 i( 0); i < ObjectEvent::OE_Count; ++i) {
			Event e( obj->GetEvent( ObjectEvent( i)));

			out << gsc_objEventNames[i] << " ";

			out << strlen( e.luaScript) << " ";

			// Output the entire string so I know how long it'll be when read back in.
			// If I didn't do this, then I wouldn't have a definitive 'end' to the script unless I
			// kept checking each word to see if it was a keyword for something else. Particularly
			// as the scripts may contain new lines.
			//const char* script( reinterpret_cast<const char*>( e.luaScript));
			//out.write( script, scriptLength);
			out << e.luaScript;

			out << "\n";
		}
	}

	out.close();

	return true;
}

/**
* Load version 100.
*
* @param file :: The file to load the interface from.
*
* @return True if no errors occurred.
*/
bool InterfaceContainer::LoadUsing_100( const char* file) {
	std::ifstream inf( file);
	s32 scriptMaxLength( gsc_luaScriptLength);

	// When loading in multiple interfaces into the container it's likely (... definite) that some
	// I.D.s will be the same in each. This 'offsets' the I.D.s of subsequent interfaces by adding
	// the number of current objects to the I.D. of objects in subsequent interfaces.
	s32 previousObjCount( this->GetNumberOfObjects());

	if( !inf.is_open())
		return false;

	char* buffer = new char[gsc_luaScriptLength];
	s32 objCount( 0);

	// Ignore the version number.
	inf.getline( buffer, scriptMaxLength);

	// Get the number of objects.
	inf.getline( buffer, scriptMaxLength);
	objCount = atoi( buffer);

	// Clamp the object count so I don't try and load in more objects than I'm allowed.
	objCount = Clamp( objCount, objCount, OBJ_COUNT);

	// Keep looping through all of the objects.
	for( s32 i( 0); i < objCount; ++i) {
		InterfaceObject* obj( nullptr);
		s32 objID( -1);
		s32 objType( 0);
		u32 fontColour( 0);
		s32 textLen( 0);
		float vec[3];
		s32 eventCount( 0);

		// Object ID.
		inf.getline( buffer, scriptMaxLength);
		objID = atoi( buffer);

		// Object type.
		inf.getline( buffer, scriptMaxLength);
		objType = atoi( buffer);

		// Create the object; the rest of the information can be directly saved into the object
		// instead of in temporary variables.
		obj = new InterfaceObject( objID + previousObjCount, InterfaceObjectType( objType));

		// Object's name.
		inf.getline( buffer, scriptMaxLength);
		obj->SetName( buffer);

		// Object's texture's file path.
		inf.getline( buffer, scriptMaxLength);
		obj->CreateTexture( buffer);

		// Object's text.
		inf >> buffer;
		textLen = atoi( buffer);
		inf.ignore( 1);
		if( textLen == 0) {
			buffer[0] = '\0';
			inf.ignore( 1);
		}
		else {
			inf.get( buffer, textLen + 1);
		}
		obj->SetText( buffer);

		// Object's font's colour.
		inf >> fontColour;
		obj->SetFontColour( fontColour);
		inf.ignore( 1);

		// Object's position.
		for( u8 i( 0); i < 3; ++i)
			inf >> vec[i];
		obj->MoveTo( vec[0], vec[1], vec[2]);
		
		// Object's scale.
		for( u8 i( 0); i < 3; ++i)
			inf >> vec[i];
		obj->ScaleTo( vec[0], vec[1], vec[2]);
		
		// Ignore the 'endline' after the scale.
		inf.ignore( 1);

		// Number of events.
		inf.getline( buffer, scriptMaxLength);
		eventCount = atoi( buffer);

		for( s32 e( 0); e < eventCount; ++e) {
			char eventName[256];
			s32 scriptLen( 0);

			// Get the name of the event.
			//inf.getline( eventName, 256);
			inf >> eventName;

			// Trim the space.
			//eventName[strlen( eventName) - 1] = '\0';

			// Get the length of the script.
			inf >> buffer;
			scriptLen = atoi( buffer);
			inf.ignore( 1);

			// Read in all the possible characters of the script.
			if( scriptLen == 0) {
				buffer[0] = '\0';
				inf.ignore( 1);
			}
			else {
				inf.readsome( buffer, scriptLen + 1);
				buffer[scriptLen+1] = 0;
			}

			// Work out which event was found.
			s32 eventIndex( 0);
			for( eventIndex; eventIndex < OE_Count; ++eventIndex)
				if( strcmp( eventName, gsc_objEventNames[eventIndex]) == 0)
					break;

			// Add the event to the object.
			if( eventIndex != OE_Count) {
				Event thisEvent( buffer);
				obj->SetEvent( (ObjectEvent)eventIndex, thisEvent);
			}

			// If something went wrong when parsing the file, exit the loop.
			if( inf.eof())
				break;
			if( inf.fail())
				break;
			if( inf.bad())
				break;
		}

		// Add the object to the interface.
		this->AddNewObject( obj);
	}

	inf.close();
	SafeDeleteArray( buffer);

	return true;
}

s32 InterfaceContainer::LuaFunction_ContainerDeleteObject() {
	if( gsp_interfaceBeingUpdated == nullptr)
		return 0;

	s32 noOfParams( 0);
	s32 objectID( -1);

	if( !MGR_LUA().GetNumberOfParameters( noOfParams, 1))
		return 0;

	if( !MGR_LUA().GetParameter( 1, LRT_Int, objectID))
		return 0;
	if( objectID == 0)
		objectID = gsp_interfaceBeingUpdated->GetObjectBeingUpdatedID();

	gsp_interfaceBeingUpdated->DeleteObject( objectID);

	return 0;
}

s32 InterfaceContainer::LuaFunction_ContainerDeleteAllObjects() {
	if( gsp_interfaceBeingUpdated == nullptr)
		return 0;
	
	gsp_interfaceBeingUpdated->DeleteAllObjects();

	return 0;
}

s32 InterfaceContainer::LuaFunction_ContainerLoadInterface() {
	if( gsp_interfaceBeingUpdated == nullptr)
		return 0;
	
	s32 noOfParams( 0);
	const char* file( nullptr);

	if( !MGR_LUA().GetNumberOfParameters( noOfParams, 1))
		return 0;

	if( !MGR_LUA().GetParameterString( 1, file))
		return 0;

	gsp_interfaceBeingUpdated->LoadInterface( file);

	return 0;
}

/**
* Sets the texture of the specified object.
*
* @return The number of objects pushed onto the stack (i.e. returned).
*/
s32 InterfaceContainer::LuaFunction_ContainerSetTexture() {
	if( gsp_interfaceBeingUpdated == nullptr)
		return 0;

	s32 noOfParams( 0);
	s32 objectID( -1);
	const char* texture( nullptr);

	if( !MGR_LUA().GetNumberOfParameters( noOfParams, 2))
		return 0;

	if( !MGR_LUA().GetParameter( 1, LRT_Int, objectID))
		return 0;
	if( objectID == 0)
		objectID = gsp_interfaceBeingUpdated->GetObjectBeingUpdatedID();

	if( !MGR_LUA().GetParameterString( 2, texture))
		return 0;

	InterfaceObject* obj( gsp_interfaceBeingUpdated->GetObjectByID( objectID));
	obj->CreateTexture( texture);

	return 0;
}

/**
* Sets the text of the specified object.
*
* @return The number of objects pushed onto the stack (i.e. returned).
*/
s32 InterfaceContainer::LuaFunction_ContainerSetText() {
	if( gsp_interfaceBeingUpdated == nullptr)
		return 0;

	s32 noOfParams( 0);
	s32 objectID( -1);
	const char* text( nullptr);

	if( !MGR_LUA().GetNumberOfParameters( noOfParams, 2))
		return 0;

	if( !MGR_LUA().GetParameter( 1, LRT_Int, objectID))
		return 0;
	if( objectID == 0)
		objectID = gsp_interfaceBeingUpdated->GetObjectBeingUpdatedID();

	if( !MGR_LUA().GetParameterString( 2, text))
		return 0;

	InterfaceObject* obj( gsp_interfaceBeingUpdated->GetObjectByID( objectID));
	obj->SetText( text);

	return 0;
}

/**
* 
*/
s32 InterfaceContainer::LuaFunction_ContainerSetFontColour() {
	if( gsp_interfaceBeingUpdated == nullptr)
		return 0;

	s32 noOfParams( 0);
	s32 objectID( -1);
	s32 colour[4];

	if( !MGR_LUA().GetNumberOfParameters( noOfParams, 5))
		return 0;

	if( !MGR_LUA().GetParameter( 1, LRT_Int, objectID))
		return 0;
	if( objectID == 0)
		objectID = gsp_interfaceBeingUpdated->GetObjectBeingUpdatedID();

	for( s32 i( 0); i < 4; ++i)
		if( !MGR_LUA().GetParameter( 2 + i, LRT_Int, colour[i]))
			return 0;

	InterfaceObject* obj( gsp_interfaceBeingUpdated->GetObjectByID( objectID));
	obj->SetFontColour( IntsToColour( colour[0], colour[1], colour[2], colour[3]));

	return 0;
}

s32 InterfaceContainer::LuaFunction_ContainerSetEventScript() {
	if( gsp_interfaceBeingUpdated == nullptr)
		return 0;

	s32 noOfParams( 0);
	s32 objectID( -1);
	const char* eventName( nullptr);
	const char* script( nullptr);
	s32 e( 0);

	if( !MGR_LUA().GetNumberOfParameters( noOfParams, 3))
		return 0;

	if( !MGR_LUA().GetParameter( 1, LRT_Int, objectID))
		return 0;
	if( objectID == 0)
		objectID = gsp_interfaceBeingUpdated->GetObjectBeingUpdatedID();

	if( !MGR_LUA().GetParameterString( 2, eventName))
		return 0;

	if( !MGR_LUA().GetParameterString( 3, script))
		return 0;

	for( e; e < OE_Count; ++e)
		if( strcmp( eventName, gsc_objEventNames[e]) == 0)
			break;

	if( e == OE_Count)
		return 0;

	InterfaceObject* obj( gsp_interfaceBeingUpdated->GetObjectByID( objectID));
	obj->SetEvent( ObjectEvent( e), Event( script));

	return 0;
}

s32 InterfaceContainer::LuaFunction_ContainerGetObjectID() {
	if( gsp_interfaceBeingUpdated == nullptr)
		return 0;

	s32 noOfParams( 0);
	const char* name( nullptr);
	InterfaceObject* obj( nullptr);

	if( !MGR_LUA().GetNumberOfParameters( noOfParams, 1))
		return 0;

	if( !MGR_LUA().GetParameterString( 1, name))
		return 0;

	obj = gsp_interfaceBeingUpdated->GetObjectByName( name);
	if( obj == nullptr)
		return 0;

	if( !MGR_LUA().ReturnValue( LRT_Int, obj->GetUniqueID()))
		return 0;

	return 1;
}

s32 InterfaceContainer::LuaFunction_ContainerGetThisObjectID() {
	if( gsp_interfaceBeingUpdated == nullptr)
		return 0;

	MGR_LUA().ReturnValue( LRT_Int, gsp_interfaceBeingUpdated->GetObjectBeingUpdated()->GetUniqueID());

	return 1;
}

s32 InterfaceContainer::LuaFunction_ContainerGetName() {
	if( gsp_interfaceBeingUpdated == nullptr)
		return 0;

	s32 noOfParams( 0);
	s32 objectID( -1);
	const char* name( nullptr);

	if( !MGR_LUA().GetNumberOfParameters( noOfParams, 1))
		return 0;

	if( !MGR_LUA().GetParameter( 1, LRT_Int, objectID))
		return 0;
	if( objectID == 0)
		objectID = gsp_interfaceBeingUpdated->GetObjectBeingUpdatedID();

	InterfaceObject* obj( gsp_interfaceBeingUpdated->GetObjectByID( objectID));
	name = obj->GetName();

	if( !MGR_LUA().ReturnValueString( name))
		return 0;

	return 1;
}

s32 InterfaceContainer::LuaFunction_ContainerGetTexture() {
	if( gsp_interfaceBeingUpdated == nullptr)
		return 0;

	s32 noOfParams( 0);
	s32 objectID( -1);
	const char* texture( nullptr);

	if( !MGR_LUA().GetNumberOfParameters( noOfParams, 1))
		return 0;

	if( !MGR_LUA().GetParameter( 1, LRT_Int, objectID))
		return 0;
	if( objectID == 0)
		objectID = gsp_interfaceBeingUpdated->GetObjectBeingUpdatedID();

	InterfaceObject* obj( gsp_interfaceBeingUpdated->GetObjectByID( objectID));
	texture = obj->GetTexture()->m_fileName;

	if( !MGR_LUA().ReturnValueString( texture))
		return 0;

	return 1;
}

s32 InterfaceContainer::LuaFunction_ContainerGetText() {
	if( gsp_interfaceBeingUpdated == nullptr)
		return 0;

	s32 noOfParams( 0);
	s32 objectID( -1);
	const char* text( nullptr);

	if( !MGR_LUA().GetNumberOfParameters( noOfParams, 1))
		return 0;

	if( !MGR_LUA().GetParameter( 1, LRT_Int, objectID))
		return 0;
	if( objectID == 0)
		objectID = gsp_interfaceBeingUpdated->GetObjectBeingUpdatedID();

	InterfaceObject* obj( gsp_interfaceBeingUpdated->GetObjectByID( objectID));
	text = obj->GetText();

	if( !MGR_LUA().ReturnValueString( text))
		return 0;

	return 1;
}

//s32 InterfaceContainer::LuaFunction_ContainerGetFontColour() {
//
//}

s32 InterfaceContainer::LuaFunction_ContainerGetEventScript() {
	if( gsp_interfaceBeingUpdated == nullptr)
		return 0;

	s32 noOfParams( 0);
	s32 objectID( -1);
	const char* eventName( nullptr);
	s32 e( 0);

	if( !MGR_LUA().GetNumberOfParameters( noOfParams, 2))
		return 0;

	if( !MGR_LUA().GetParameter( 1, LRT_Int, objectID))
		return 0;
	if( objectID == 0)
		objectID = gsp_interfaceBeingUpdated->GetObjectBeingUpdatedID();

	if( !MGR_LUA().GetParameterString( 2, eventName))
		return 0;

	for( e; e < OE_Count; ++e)
		if( strcmp( eventName, gsc_objEventNames[e]) == 0)
			break;

	if( e == OE_Count)
		return 0;

	InterfaceObject* obj( gsp_interfaceBeingUpdated->GetObjectByID( objectID));
	MGR_LUA().ReturnValueString( obj->GetEvent( ObjectEvent( e)).luaScript);

	return 1;
}