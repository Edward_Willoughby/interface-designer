/********************************************************************
*
*	CLASS		:: InterfaceObject
*	DESCRIPTION	:: Contains the information pertaining to a specific object on the interface,
*					whether that's text, an icon, or a button.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2015 / 07 / 19
*
********************************************************************/

#ifndef InterfaceObjectH
#define InterfaceObjectH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include <DBEFont.h>
#include <DBEMovable.h>

namespace DBE {
	struct Texture;
}
/********************************************************************
*	Defines and constants.
********************************************************************/
/// The maximum length of a Lua script for one event.
static const s32 gsc_luaScriptLength = 1024;

enum InterfaceObjectType {
	IO_Text = 0,
	IO_Icon,
	IO_Button,
	IO_Count,
};
static const char* gsc_objTypeNames[] = {
	"Text",
	"Icon",
	"Button",
};

enum MouseButtonState {
	MBS_Inactive = 0,
	MBS_Down,
	MBS_Up,
	MBS_Count,
};

struct EventArgs {
	EventArgs()
		: keyPressed( -1)
		, keyState( MBS_Inactive)
	{}

	s32 keyPressed;
	MouseButtonState keyState;
};

enum ObjectEvent {
	OE_Enter = 0,
	OE_Hover,
	OE_Click,
	OE_Exit,
	OE_Count,
};
static const char* gsc_objEventNames[] = {
	"EventEnter",
	"EventHover",
	"EventClick",
	"EventExit",
};

struct Event {
	Event()
		: active( false)
	{
		luaScript[0] = '\0';
	}

	Event( const char* l) 
		: active( false)
	{
		strcpy_s( luaScript, gsc_luaScriptLength, l);
	}

	Event& operator=( const Event& e) {
		active = e.active;
		strcpy_s( luaScript, gsc_luaScriptLength, e.luaScript);

		return *this;
	}

	bool	active;
	char	luaScript[gsc_luaScriptLength];
};


/*******************************************************************/
class InterfaceObject : public DBE::Movable {
	public:
		/// Constructor.
		InterfaceObject( s32 id, InterfaceObjectType type);
		/// Destructor.
		~InterfaceObject();

		bool OnUpdate( float deltaTime);

		// Event handlers.
		virtual void OnEnter( EventArgs e) {}
		virtual void OnHover( EventArgs e) {}
		virtual void OnClick( EventArgs e) {}
		virtual void OnExit( EventArgs e) {}

		s32 GetUniqueID() const;

		void SetName( const char* name);
		const char* GetName() const;

		InterfaceObjectType GetType() const;
		void GetTypeString( char* buffer, s32 size) const;

		void CreateTexture( const char* texture);
		DBE::Texture* GetTexture() const;

		void SetText( const char* text);
		const char* GetText() const;

		void SetFontColour( u32 colour);
		u32 GetFontColour() const;

		DBE::Font::Style GetFontStyle() const;

		void SetEvent( ObjectEvent oe, Event e);
		Event GetEvent( ObjectEvent oe) const;

		DBE::Vec2 GetBoundingDimensions() const;
		bool MouseIsOverObject( const DBE::Vec2& mousePos);

		static void UseLuaScripts( bool useScripts);
		static void EnableEvents( bool enableEvents);

		/// Compares two 'InterfaceObject's to determine which is closer.
		static bool Closer( const InterfaceObject* a, const InterfaceObject* b);
		
	private:
		void InternalOnEnter( EventArgs e);
		void InternalOnHover( EventArgs e);
		void InternalOnClick( EventArgs e);
		void InternalOnExit( EventArgs e);

		const s32	m_uniqueID;
		char		m_name[256];

		InterfaceObjectType m_type;

		DBE::Texture*		mp_texture;		// Icon & Button
		char				m_text[256];	// Button & Text
		DBE::Font::Style	m_fontStyle;	// Button & Text
		
		Event m_events[OE_Count];
		
		/// Private copy constructor to prevent accidental multiple instances.
		InterfaceObject( const InterfaceObject& other);
		/// Private assignment operator to prevent accidental multiple instances.
		InterfaceObject& operator=( const InterfaceObject& other);
		
};

/*******************************************************************/
#endif	// #ifndef InterfaceObjectH
