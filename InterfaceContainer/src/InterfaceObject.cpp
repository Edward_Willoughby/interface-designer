/********************************************************************
*	Function definitions for the InterfaceObject class.
********************************************************************/

/********************************************************************
*	Include the header file.
********************************************************************/
#include "InterfaceObject.h"

#include <DBEApp.h>
#include <DBECollision.h>
#include <DBEFont.h>
#include <DBETextureMgr.h>
/********************************************************************
*	Namespaces, defines, constants, and local variables.
********************************************************************/
using namespace DBE;

/// Static variable for whether or not scripts should be executed.
static bool gs_useScripts = true;
static bool gs_enableEvents = true;


/**
* Constructor.
*/
InterfaceObject::InterfaceObject( s32 id, InterfaceObjectType type)
	: m_uniqueID( id)
	, m_type( type)
	, mp_texture( nullptr)
	, m_fontStyle( WHITE)
{
	m_name[0] = '\0';
	m_text[0] = '\0';

	char buffer[256];
	DebuggerDependentPath( buffer, sizeof( buffer), "DBEngine_ROOT", "", "res\\textures\\texture_missing.dds");
	mp_texture = MGR_TEXTURE().LoadTexture( buffer, GET_APP()->GetSamplerState());
}

/**
* Destructor.
*/
InterfaceObject::~InterfaceObject() {
	MGR_TEXTURE().DeleteTexture( mp_texture);
}

/**
* 
*/
bool InterfaceObject::OnUpdate( float deltaTime) {
	if( !gs_enableEvents)
		return true;

	bool mouseOverPreviously( m_events[OE_Enter].active || m_events[OE_Hover].active);
	bool mouseOver( false);
	Vec2 mousePos;

	if( !MGR_INPUT().GetMousePos( mousePos))
		mouseOver = false;
	else
		mousePos = MGR_INPUT().ConvertMousePosToScreenCoordinates( mousePos);

	if( MGR_INPUT().KeyPressed( 'F') && m_type == IO_Icon)
		mouseOver = mouseOver;

	if( mouseOver = this->MouseIsOverObject( mousePos)) {
		// 'OnEnter' if the mouse WASN'T over it previously and is now.
		if( !mouseOverPreviously && mouseOver) {
			m_events[OE_Enter].active = true;
			EventArgs eo;
			this->InternalOnEnter( eo);
		}
		else {
			m_events[OE_Enter].active = false;
		}

		// 'OnHover' if the mouse was over it previously AND is now.
		if( mouseOverPreviously && mouseOver)
			m_events[OE_Hover].active = true;

		if( m_events[OE_Hover].active) {
			EventArgs eh;
			this->InternalOnHover( eh);
		}

		bool mouseClicks[2] = { false, false };
		mouseClicks[0] = MGR_INPUT().KeyPressed( VK_LBUTTON);
		mouseClicks[1] = MGR_INPUT().KeyPressed( VK_RBUTTON);
		if( mouseClicks[0] || mouseClicks[1]) {
			EventArgs ec;
			ec.keyPressed = mouseClicks[0] ? VK_LBUTTON : VK_RBUTTON;
			ec.keyState = MouseButtonState::MBS_Down;
			this->InternalOnClick( ec);
		}
	}
	else {
		// 'OnExit' if the mouse was over it previously and isn't now.
		if( mouseOverPreviously) {
			EventArgs ee;
			this->InternalOnExit( ee);
		}

		m_events[OE_Hover].active = false;
	}

	return true;
}

/**
* 
*/
s32 InterfaceObject::GetUniqueID() const {
	return m_uniqueID;
}

/**
* 
*
* @param :: 
*
* @return 
*/
void InterfaceObject::SetName( const char* name) {
	strcpy_s( m_name, 256, name);
}

/**
* 
*/
const char* InterfaceObject::GetName() const {
	return m_name;
}

/**
* 
*/
void InterfaceObject::CreateTexture( const char* texture) {
	MGR_TEXTURE().DeleteTexture( mp_texture);

	mp_texture = MGR_TEXTURE().LoadTexture( texture, GET_APP()->GetSamplerState());
}

/**
* 
*/
DBE::Texture* InterfaceObject::GetTexture() const {
	return mp_texture;
}

/**
* 
*/
InterfaceObjectType InterfaceObject::GetType() const {
	return m_type;
}

/**
* 
*/
void InterfaceObject::GetTypeString( char* buffer, s32 size) const {
	const char* type = gsc_objTypeNames[m_type];

	strcpy_s( buffer, size, type);
}

/**
* 
*/
void InterfaceObject::SetText( const char* text) {
	strcpy_s( m_text, 256, text);
}

/**
* 
*/
const char* InterfaceObject::GetText() const {
	return m_text;
}

/**
* 
*/
void InterfaceObject::SetFontColour( u32 colour) {
	m_fontStyle.colour = colour;
}

/**
* 
*/
u32 InterfaceObject::GetFontColour() const {
	const VertColour* c( &m_fontStyle.colour);
	return IntsToColour( c->red, c->green, c->blue, c->alpha);
}

/**
* 
*/
DBE::Font::Style InterfaceObject::GetFontStyle() const {
	return m_fontStyle;
}

/**
* 
*/
void InterfaceObject::SetEvent( ObjectEvent oe, Event e) {
	DBE_Assert( oe >= 0 && oe < OE_Count);

	m_events[oe] = e;
}

/**
* 
*/
Event InterfaceObject::GetEvent( ObjectEvent oe) const {
	DBE_Assert( oe >= 0 && oe < OE_Count);

	return m_events[oe];
}

/**
* 
*/
DBE::Vec2 InterfaceObject::GetBoundingDimensions() const {
	Vec2 size;

	if( m_type == InterfaceObjectType::IO_Text) {
		Vec2 sMin, sMax;
		DEFAULT_FONT()->CalculateTextDimensions( m_text, this->GetPosition(), sMin, sMax);

		size = sMax - sMin;
	}
	else {
		size = this->GetScale().GetXY();
	}

	return size;
}

/**
* 
*/
bool InterfaceObject::MouseIsOverObject( const DBE::Vec2& mousePos) {
	Vec2 sMin, sMax;

	if( m_type == InterfaceObjectType::IO_Text) {
		DEFAULT_FONT()->CalculateTextDimensions( m_text, this->GetPosition(), sMin, sMax);
	}
	else {
		float sx( this->GetScale().GetX() / 2.0f);
		float sy( this->GetScale().GetY() / 2.0f);
		sMin = Vec2( this->GetPosition().GetX() - sx, this->GetPosition().GetY() - sy);
		sMax = Vec2( this->GetPosition().GetX() + sx, this->GetPosition().GetY() + sy);
	}

	return Collision::IntersectPointSquare( mousePos, sMin, sMax);
}

/**
* 
*/
void InterfaceObject::UseLuaScripts( bool useScripts) {
	gs_useScripts = useScripts;
}

/**
* 
*/
void InterfaceObject::EnableEvents( bool enableEvents) {
	gs_enableEvents = enableEvents;
}

/**
* Compares two 'InterfaceObject's to determine which is closer.
*/
bool InterfaceObject::Closer( const InterfaceObject* a, const InterfaceObject* b)
{
	float zThis( a->GetPosition().GetZ());
	float zOther( b->GetPosition().GetZ());

	return zThis < zOther;
}

void InterfaceObject::InternalOnEnter( EventArgs e) {
	//DebugTrace( "Object %s: OnEnter\n", m_name);

	if( gs_useScripts)
		MGR_LUA().ExecuteCode( m_events[OE_Enter].luaScript);

	this->OnEnter( e);
}

void InterfaceObject::InternalOnHover( EventArgs e) {
	//DebugTrace( "Object %s: OnHover\n", m_name);

	if( gs_useScripts)
		MGR_LUA().ExecuteCode( m_events[OE_Hover].luaScript);

	this->OnHover( e);
}

void InterfaceObject::InternalOnClick( EventArgs e) {
	//DebugTrace( "Object %s: Click (%s, %i)\n", m_name, e.keyPressed == VK_LBUTTON ? "Left" : "Right", e.keyState == MouseButtonState::MBS_Down);

	if( gs_useScripts)
		MGR_LUA().ExecuteCode( m_events[OE_Click].luaScript);

	this->OnClick( e);
}

void InterfaceObject::InternalOnExit( EventArgs e) {
	//DebugTrace( "Object %s: OnExit\n", m_name);

	if( gs_useScripts)
		MGR_LUA().ExecuteCode( m_events[OE_Exit].luaScript);

	this->OnExit( e);
}