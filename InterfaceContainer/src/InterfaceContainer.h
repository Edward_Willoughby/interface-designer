/********************************************************************
*
*	CLASS		:: InterfaceContainer
*	DESCRIPTION	:: Contains and maintains all of the components on the interface.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2015 / 07 / 18
*
********************************************************************/

#ifndef InterfaceContainerH
#define InterfaceContainerH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include <list>

#include <DBEColour.h>
#include <DBEMath.h>
#include <DBERandom.h>

namespace DBE {
	class App;
	class Camera;
	class Font;
	class UITexture;
}
class InterfaceObject;
/********************************************************************
*	Defines and constants.
********************************************************************/
static const s32 OBJ_COUNT = 100;

struct InterfaceFuncPointers {
	typedef void (*FPObjectDeleted)();
	typedef void (*FPObjectUpdate)( InterfaceObject**);

	InterfaceFuncPointers()
		: objectDeleted( nullptr)
		, objectUpdate( nullptr)
	{}

	FPObjectDeleted objectDeleted;
	FPObjectUpdate	objectUpdate;
};

typedef std::list<InterfaceObject*> ContainerObjectList;


/*******************************************************************/
class InterfaceContainer {
	public:
		/// Constructor.
		InterfaceContainer() {}

		bool Init( DBE::App* p_app, InterfaceFuncPointers funcPointers);
		bool Update( float deltaTime);
		bool Render( float deltaTime);
		void Shutdown();

		/// Saves the current interface to the specified file.
		bool SaveInterface( const char* file) const;
		/// Loads an interface from the specified file.
		bool LoadInterface( const char* file);

		s32 GetNumberOfObjects() const;
		InterfaceObject* GetObjectAt( s32 index) const;

		/// Adds a new object to the container.
		bool AddNewObject( InterfaceObject* p_obj);
		/// Deletes an existing object in the container.
		bool DeleteObject( s32 id);
		/// Deletes all of the existing objects in the container.
		void DeleteAllObjects();

		s32 GetAvailableUniqueID() const;

		InterfaceObject* GetObjectByID( s32 id);
		InterfaceObject* GetObjectByName( const char* name);

		InterfaceObject* GetObjectBeingUpdated();
		s32 GetObjectBeingUpdatedID();

		ContainerObjectList::const_iterator GetConstIterator() const;
		ContainerObjectList::const_reverse_iterator GetConstRIterator() const;
		void SortObjectsByZDistance();

		/// Enables/Disables the objects executing their Lua event scripts.
		void UseLuaScripts( bool useScripts);
		void EnableObjectEvents( bool enableEvents);

	private:
		/// Save version 100.
		bool SaveUsing_100( const char* file) const;
		/// Load version 100.
		bool LoadUsing_100( const char* file);

		// Lua functions that should always be available as they modify the objects or container.
		static s32 LuaFunction_ContainerDeleteObject();
		static s32 LuaFunction_ContainerDeleteAllObjects();
		static s32 LuaFunction_ContainerLoadInterface();

		static s32 LuaFunction_ContainerSetTexture();
		static s32 LuaFunction_ContainerSetText();
		static s32 LuaFunction_ContainerSetFontColour();
		static s32 LuaFunction_ContainerSetEventScript();

		static s32 LuaFunction_ContainerGetObjectID();
		static s32 LuaFunction_ContainerGetThisObjectID();
		static s32 LuaFunction_ContainerGetName();
		static s32 LuaFunction_ContainerGetTexture();
		static s32 LuaFunction_ContainerGetText();
		//static s32 LuaFunction_ContainerGetFontColour();
		static s32 LuaFunction_ContainerGetEventScript();

		DBE::App*				mp_app;
		InterfaceFuncPointers	m_fp;

		DBE::UITexture*	mp_icon;
		DBE::Camera*	mp_cam;

		ContainerObjectList	mp_objects;
		s32					m_objBeingUpdated;

		/// Private copy constructor to prevent multiple instances.
		InterfaceContainer( const InterfaceContainer&);
		/// Private assignment operator to prevent multiple instances.
		InterfaceContainer& operator=( const InterfaceContainer&);

};

/*******************************************************************/
#endif	// #ifndef InterfaceContainerH