/********************************************************************
*	Function definitions for the InterfaceDesignerPlayer class.
********************************************************************/

/********************************************************************
*	Include the header file.
********************************************************************/
#include "InterfaceDesignerPlayer.h"

#include <shellapi.h>

#include <DBEColour.h>

#include <InterfaceContainer.h>
#include <InterfaceObject.h>

/********************************************************************
*	Defines, constants, namespaces, and local variables.
********************************************************************/
using namespace DBE;

static u32 gs_frames = 0;
static float gs_deltaTotal = 0.0f;


/**
* Deals with pre-initialisation.
*
* @return True if there were no errors.
*/
bool InterfaceDesignerPlayer::HandlePreInit() {
	this->GetInterfaceFile();

	return true;
}

/**
* Deals with initialisation.
*
* @return True if everything initialised correctly.
*/
bool InterfaceDesignerPlayer::HandleInit() {
	// Prevent the texture manager from deleted textures that are no longer used.
	MGR_TEXTURE().DeleteUnreferencedTextures( false);

	mp_interface = new InterfaceContainer();
	if( !mp_interface->Init( this, InterfaceFuncPointers()))
		return false;

	if( !mp_interface->LoadInterface( m_file))
		return false;

	this->EnableLightDirectional( 0, Vec3( -1.0f, -1.0f, -1.0f), Vec3( 1.0f, 1.0f, 1.0f));

	m_wireframe = false;

	m_windowTitle = "Interface Designer Player";
	this->SetWindowTitle( m_windowTitle);

	this->SetFocusResponse( FocusResponse::FR_SlowRendering);

	return true;
}

/**
* Deals with any input.
*
* @return True if nothing went wrong.
*/
bool InterfaceDesignerPlayer::HandleInput() {
	// Toggle wireframe mode.
	if( MGR_INPUT().KeyPressed( 'W'))
		m_wireframe = !m_wireframe;

	// Quit the program.
	if( MGR_INPUT().KeyPressed( VK_ESCAPE))
		this->Terminate();

	static bool s_capFPS( true);
	if( MGR_INPUT().KeyPressed( 'F')) {
		s_capFPS = !s_capFPS;

		if( s_capFPS)
			FRAME_TIMER().SetFPS( true, 60);
		else
			FRAME_TIMER().SetFPS( false);
	}

	return true;
}

/**
* Deals with any updating.
*
* @param deltaTime :: The time taken to render the last frame.
*
* @return True if everything updated correctly.
*/
bool InterfaceDesignerPlayer::HandleUpdate( float deltaTime) {
	// Set the window title.
	char buffer[256];
	sprintf_s( buffer, sizeof( buffer), "%s - FPS: %f, Delta: %f", m_windowTitle, FRAME_TIMER().FPS(), FRAME_TIMER().DeltaTime());
	this->SetWindowTitle( buffer);

	mp_interface->Update( deltaTime);

	++gs_frames;
	gs_deltaTotal += deltaTime;

	return true;
}

/**
* Deals with any rendering.
*
* @param deltaTime :: The time taken to render the last frame.
*
* @return True if everything rendered correctly.
*/
bool InterfaceDesignerPlayer::HandleRender( float deltaTime) {
	mp_interface->Render( deltaTime);

	return true;
}

/**
* Deals with anything that needs to be released or shutdown.
*
* @return True if everything was closed correctly.
*/
bool InterfaceDesignerPlayer::HandleShutdown() {
	mp_interface->Shutdown();
	SafeDelete( mp_interface);

	// If the program was started by the tool then delete the temporary file.
	if( m_temporaryFile)
		remove( m_file);

	DebugTrace( "Frames: %i\nDelta Average: %f\nAverage F.P.S.: %f\n", gs_frames, gs_deltaTotal / gs_frames, gs_frames / gs_deltaTotal);

	return true;
}

/**
* Gets the '.dbif' file to run.
* NOTE: The destination array should be at least 256 large to accomodate any potential file.
*
* @dest		:: The variable to hold the path to the file.
* @destSize	:: The size of the 'dest' array.
*/
void InterfaceDesignerPlayer::GetInterfaceFile() {
	LPWSTR* commands;
	s32 cmdCount( 0);
	bool passedAsArg( false);

	// Clear the array.
	m_file[0] = 0;

	// Get the commands sent to this program on startup.
	commands = CommandLineToArgvW( GetCommandLineW(), &cmdCount);

	if( cmdCount > 1) {
		// The first argument SHOULD be the path to the interface file.
		sprintf_s( m_file, sizeof( m_file), "%ws", commands[1]);

		s32 len( strlen( m_file));
		if( len > 10 && strcmp( ".dbif.temp", &m_file[len-10]) == 0) {
			passedAsArg = true;
			m_temporaryFile = true;
		}
		else if( len > 5 && strcmp( ".dbif", &m_file[len-5]) == 0) {
				passedAsArg = true;
		}

		if( cmdCount > 4) {
			char buffer[256];
			s32 resX( 0), resY( 0);
			bool fullScreen( false);

			sprintf_s( buffer, sizeof( buffer), "%ws", commands[2]);
			resX = atoi( buffer);

			sprintf_s( buffer, sizeof( buffer), "%ws", commands[3]);
			resY = atoi( buffer);

			sprintf_s( buffer, sizeof( buffer), "%ws", commands[4]);
			fullScreen = (strcmp( buffer, "True") == 0);

			if( fullScreen)
				this->SetWindowDimensions( 0, 0);
			else
				this->SetWindowDimensions( resX, resY);
		}
	}
	
	// If the parameter was incorrect or none was sent, then load the specified file from the
	// config file.
	//if( !passedAsArg)
	//	strcpy_s( dest, destSize, "res/interfaces/interfaceTest.dbif");
}