/********************************************************************
*
*	CLASS		:: InterfaceDesignerPlayer
*	DESCRIPTION	:: Runs a file created by the 'Interface Designer'.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2015 / 07 / 18
*
********************************************************************/

#ifndef InterfaceDesignerPlayerH
#define InterfaceDesignerPlayerH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include <DBEApp.h>
#include <DBEColour.h>

class InterfaceContainer;
/********************************************************************
*	Defines and constants.
********************************************************************/


/*******************************************************************/
class InterfaceDesignerPlayer : public DBE::App {
	public:
		/// Constructor.
		InterfaceDesignerPlayer() {}

		/// Deals with pre-initialisation.
		bool HandlePreInit();
		/// Deals with initialisation.
		bool HandleInit();
		/// Deals with any input.
		bool HandleInput();
		/// Deals with any updating.
		bool HandleUpdate( float deltaTime);
		/// Deals with any rendering.
		bool HandleRender( float deltaTime);
		/// Deals with anything that needs to be released or shutdown.
		bool HandleShutdown();

	private:
		/// Gets the '.dbif' file to run.
		void GetInterfaceFile();

		const char* m_windowTitle;

		InterfaceContainer* mp_interface;

		bool m_temporaryFile;
		char m_file[256];

		bool m_wireframe;

		/// Private copy constructor to prevent multiple instances.
		InterfaceDesignerPlayer( const InterfaceDesignerPlayer&);
		/// Private assignment operator to prevent multiple instances.
		InterfaceDesignerPlayer& operator=( const InterfaceDesignerPlayer&);

};

APP_MAIN( InterfaceDesignerPlayer, BLUE);

/*******************************************************************/
#endif	// #ifndef InterfaceDesignerPlayerH